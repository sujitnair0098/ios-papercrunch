//
//  SA_MANAGER.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 14/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SA_MANAGER : NSObject

+(SA_MANAGER *)sharedManager;

@property(nonatomic,retain) NSMutableArray *arrUsers;
@property(nonatomic,retain) NSMutableArray *arrPapers;
@property(nonatomic,strong) NSString *deviceToken;
@property(nonatomic,strong) NSMutableArray *arrChat;
@property(nonatomic,strong) NSMutableArray *arrMessagedata;
@end
