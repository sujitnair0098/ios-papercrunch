//
//  Message.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 23/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject
@property(nonatomic,strong) NSString *strMessage;
@property(nonatomic,strong) NSNumber *strMessageid;
@property(nonatomic,strong) NSString *strReceiverid;
@property(nonatomic,strong) NSString *strSenderid;
@property(nonatomic,strong) NSString *strSentdate;

-(id)initWithDict:(NSDictionary*)dict;
@end
