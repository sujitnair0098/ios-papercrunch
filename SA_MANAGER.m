//
//  SA_MANAGER.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 14/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "SA_MANAGER.h"
#import "User.h"


@class User;

@implementation SA_MANAGER
+(SA_MANAGER *)sharedManager{
    
    static SA_MANAGER *_sharedManager=nil;
    
    //static dispatch_once_t oncePredicate;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager=[[SA_MANAGER alloc]init];
        
    });
    
    return _sharedManager;
}


-(id)init{
    
    self=[super init];
    
    if(self){
        
        self.arrUsers=[[NSMutableArray alloc]init];
        self.arrPapers=[[NSMutableArray alloc]init];
        self.deviceToken=[[NSString alloc]init];
        self.arrChat=[[NSMutableArray alloc]init];
        self.arrMessagedata=[[NSMutableArray alloc]init];
    }
    
    return self;
    
    
}

@end
