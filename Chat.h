//
//  Chat.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Chat : NSObject

@property (nonatomic,assign) NSNumber *userid;
@property (nonatomic,strong) NSString *username;
@property (nonatomic,strong) NSString *useremail;

-(id)initWithDict:(NSDictionary*)dict;

@end
