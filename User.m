//
//  User.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 14/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "User.h"

@implementation User

-(id)initWithDict:(NSDictionary*)dict
{
    self = [super init];
    
    if(self)
    {
        self.username=[dict objectForKey:@"username"];
        self.userfacebookid=[dict objectForKey:@"userfacebookid"];
        self.useremail=[dict objectForKey:@"useremail"];
        self.imageurl=[dict objectForKey:@"imageurl"];
        self.devicetoken=[dict objectForKey:@"devicetoken"];
        self.firstname=[dict objectForKey:@"firstname"];
        self.userId=[dict objectForKey:@"userId"];
        self.lastname=[dict objectForKey:@"lastname"];
      
    }
    return self;
}

@end
