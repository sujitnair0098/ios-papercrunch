//
//  User.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 14/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(nonatomic,strong) NSString *username;
@property(nonatomic,assign) NSNumber *userfacebookid;
@property(nonatomic,strong) NSString *useremail;
@property(nonatomic,strong) NSString *imageurl;
@property(nonatomic,strong) NSString *devicetoken;
@property(nonatomic,strong) NSString *firstname;
@property(nonatomic,strong) NSString *lastname;
@property(nonatomic,assign) NSNumber *userId;

-(id)initWithDict:(NSDictionary*)dict;

@end
