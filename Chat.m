//
//  Chat.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "Chat.h"

@implementation Chat
-(id)initWithDict:(NSDictionary*)dict
{
    self = [super init];
    
    if(self)
    {
        self.username=[dict objectForKey:@"username"];
        self.userid=[dict objectForKey:@"userid"];
        self.useremail=[dict objectForKey:@"useremail"];
            }
    return self;
}
@end
