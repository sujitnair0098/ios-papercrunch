//
//  Message.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 23/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "Message.h"

@implementation Message
-(id)initWithDict:(NSDictionary*)dict
{
    self = [super init];
    
    if(self)
    {
        self.strMessage=[dict objectForKey:@"strMessage"];
        self.strMessageid=[dict objectForKey:@"strMessageid"];
        self.strReceiverid=[dict objectForKey:@"strReceiverid"];
        self.strSenderid=[dict objectForKey:@"strSenderid"];
        self.strSentdate=[dict objectForKey:@"strSentdate"];
    }
    return self;
}
@end
