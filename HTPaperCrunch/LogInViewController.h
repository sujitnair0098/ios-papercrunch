//
//  LogInViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertController+Blocks.h"
#import "SignUpViewController.h"
#import "User.h"
#import "SA_MANAGER.h"


@interface LogInViewController : UIViewController<UITextFieldDelegate, UIScrollViewDelegate>
{
    IBOutlet UIButton *btnLogin;
    IBOutlet UIButton *btnSignup;
    UIAlertController * alertController;
    UITextField *activeTextField;
}

@property(nonatomic,strong) IBOutlet UITextField *txtEmailId;
@property(nonatomic,strong) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_login;
@property (strong, nonatomic) IBOutlet UIView *view_login;

-(IBAction)btnSigninClicked:(id)sender;
-(IBAction)btnSignupClicked:(id)sender;


@end
