//
//  TutorViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondViewController.h"

@interface TutorViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageViewTutor;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewProgress;

- (IBAction)didSkip:(id)sender;
- (IBAction)didNext:(id)sender;




@end
