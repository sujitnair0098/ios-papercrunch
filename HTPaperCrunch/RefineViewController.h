//
//  RefineViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 24/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BranchViewController.h"
#import "PaperViewController.h"

@interface RefineViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{

//    IBOutlet UIButton *btnCancel;
//    IBOutlet UIButton *btnRefine;
//    IBOutlet UIButton *btnApply;
    NSMutableArray *arrRefineCateogory;

}

@property(nonatomic,weak)IBOutlet UITableView *tblRefine;
@property(nonatomic,weak) IBOutlet UISegmentedControl *segFilterSort;
@property(nonatomic,strong) IBOutlet UIView *vw;
-(IBAction)btnCancelClicked:(id)sender;
@property(nonatomic,retain) NSMutableArray *arrBranches;
@end
