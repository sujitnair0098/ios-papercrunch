//
//  ViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 16/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    

//    [self.button1 setTitle:@"I AM NEW HERE" forState:UIControlStateNormal];
//    [self.button2 setTitle:@"JUST LOG ME IN" forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapButton1:(id)sender {
    
    UIButton *resulteButton= (UIButton*)sender;
    NSString *buttonTitle=resulteButton.titleLabel.text;
    
    if ([buttonTitle isEqualToString:@"I AM NEW HERE"]) {
        TutorViewController * tutor = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorViewController"];
        [self presentViewController:tutor animated:YES completion:nil];
        
//        [self.button1 setTintColor:[UIColor colorWithRed:<#(CGFloat)#> green:<#(CGFloat)#> blue:<#(CGFloat)#> alpha:<#(CGFloat)#>]];

    }

}


- (IBAction)didTapButton2:(id)sender {
    SecondViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    //vc.str1 = [NSString stringWithFormat:@"LOGIN WITH EMAIL"];
    //vc.str2 = [NSString stringWithFormat:@"LOGIN WITH FACEBOOK"];
    
    [self presentViewController:vc animated:YES completion:nil];


}
@end
