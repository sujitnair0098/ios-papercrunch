//
//  MyPaperTableViewCell.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "MyPaperTableViewCell.h"

@implementation MyPaperTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)btnBookmrakPressed:(id)sender {
    
    
    
    if ([_btnBookmarks.imageView.image isEqual:[UIImage imageNamed:@"bookMarkempty"]]) {
        
    [_btnBookmarks setImage:[UIImage imageNamed:@"bookMarkfilled"] forState:normal];
    }
    
    else{
    
    [_btnBookmarks setImage:[UIImage imageNamed:@"bookMarkempty"] forState:normal];
    }
    
    

}

-(IBAction)btnSavePressed:(id)sender{


    if ([_btnDownload.imageView.image isEqual:[UIImage imageNamed:@"savehollow"]]) {
        [_btnDownload setImage:[UIImage imageNamed:@"savefilled"] forState:normal];
    }
    
    else{
        
        [_btnDownload setImage:[UIImage imageNamed:@"savehollow"] forState:normal];
    }
    


}

//- (void)layoutSubviews
//{
//    self.lblExam.frame=CGRectMake(77, 51, 205, 25);
//    CGRect labelFrame = self.lblExam.frame;
//   
//    labelFrame.size.height = self.frame.size.height - 75.0f;
//    self.lblExam.frame = labelFrame;
//    
//    CGRect buttonFrame = self.btnSeemore.frame;
//    buttonFrame.origin.y = labelFrame.origin.y+labelFrame.size.height-25.0f;
//    buttonFrame.origin.x= labelFrame.origin.x+200.0f;
//    self.btnSeemore.frame = buttonFrame;
//}


@end
