//
//  SecondViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "SignUpViewController.h"
#import "LogInViewController.h"
#import "HomeViewController.h"
#import "FBSDKLoginKit.h"
#import "FBSDKCoreKit.h"
#import "SA_MANAGER.h"
#import "User.h"
#import "MBProgressHUD.h"

@interface SecondViewController : UIViewController
{
    FBSDKLoginButton * loginButton;
}

@property (strong, nonatomic) NSString * str1;
@property (strong, nonatomic) NSString * str2;
@property(nonatomic,strong) NSString*strnm;

@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;

- (IBAction)didTapButton1:(id)sender;
- (IBAction)didTapButton2:(id)sender;

@end
