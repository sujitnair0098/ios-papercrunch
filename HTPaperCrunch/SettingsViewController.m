//
//  SettingsViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 26/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    settingsArray = @[@"Notifications",@"Bookmarks",@"Recently Viewed Papers",@"Rate us on the Play Store",@"Send feedback",@"Suggest us a College",@"About"];
    
    //images are resolutions of 75X75.
    settingsImageArray = @[@"s_notifications.png",@"s_bookmark.png",@"s_recent.png",@"s_rateUs.png",@"s_feedback.png",@"s_suggest.png",@"s_about.png"];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return settingsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.textLabel.text = [settingsArray objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[settingsImageArray objectAtIndex:indexPath.row]];
    
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
