//
//  MyPapersViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "MyPapersViewController.h"

@interface MyPapersViewController ()
{
    UIButton *buttonClose;
}
@end

@implementation MyPapersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"MyPaperTableViewCell" bundle:nil];
    [self.tblPapers registerNib:cellNib forCellReuseIdentifier:@"cellid"];
    // Do any additional setup after loading the view.
    
    arrPapers=[[NSMutableArray alloc]initWithObjects:@"COMMUNICATION SKILLS",@"APPLIED MATHEMATICS",@"DBMS",@"NETWORK SECURITY",@"APPLIED MECHANICS",@"ENTREPNERSHIP",@"NUCLEAR PHYSICS",@"ARTIFICIAL INTELLIGENCE", nil];
    arrExam=[[NSMutableArray alloc]initWithObjects:@"FE Sem1-May 2013", nil];
    
    dictPapers=[[NSMutableDictionary alloc]init];
    webVw=[[UIWebView alloc]init];
    
    webVw.delegate=self;
    
        
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrPapers.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellid=@"cellid";
    MyPaperTableViewCell *cell=[_tblPapers dequeueReusableCellWithIdentifier:@"cellid"];
    
    if (cell==nil) {
        cell=[[MyPaperTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    
//    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
//    cell.lblExam.numberOfLines = isSelected?0:1;
    
    cell.imgPaper.image= [UIImage imageNamed:@"50-50paper.png"];
    cell.lblTitle.text=[arrPapers objectAtIndex:indexPath.row];
    cell.lblExam.text=[arrExam objectAtIndex:0];
    
//    NSString *buttonTitle = isSelected?@"Less":@"More";
//    [cell.btnSeemore setTitle:buttonTitle forState:UIControlStateNormal];
//    [cell.btnSeemore addTarget:self action:@selector(seeMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.btnSeemore setTag:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    cell.tag=indexPath.row;
//    [cell.btnBookmarks addTarget:self action:@ selector(bookmarkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
     //cell.iRecordIndex = indexPath.row;
   return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
//    
//    CGFloat maxHeight = MAXFLOAT;
//    CGFloat minHeight = 40.0f;
//    
//    CGFloat constrainHeight = isSelected?maxHeight:minHeight;
//    CGFloat constrainWidth  = 205.0f;
//    
//    NSString *text       = [arrExam objectAtIndex:0];
//    CGSize constrainSize = CGSizeMake(constrainWidth, constrainHeight);
//    CGSize labelSize     = [text sizeWithFont:[UIFont systemFontOfSize:15.0f]
//                            constrainedToSize:constrainSize
//                                lineBreakMode:NSLineBreakByCharWrapping];
//    
//    return MAX(labelSize.height+75, 100.0f);
    
    return 100;

    
}


- (void)seeMoreButtonPressed:(UIButton *)button
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    [self addOrRemoveSelectedIndexPath:indexPath];
}

- (void)addOrRemoveSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedIndexPaths removeObject:indexPath];
    }else{
        [self.selectedIndexPaths addObject:indexPath];
    }
    
    [self.tblPapers reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationFade];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    // NSString *urlString = @"https://www.papercrunch.in/paper?link=1/2010/MAY/1-2010-may-1.6.pdf";
    NSString *urlString=@"https://www.google.com/url?q=https%3A%2F%2Fwww.papercrunch.in%2FexamLibrary%2FCI3%2F2010%2FDEC%2Fci3-2010-dec-ci3.6.pdf&sa=D&sntz=1&usg=AFQjCNGo6DKI20FO_MwamAn9FZwbLxJxCQ";
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        webVw.frame=CGRectMake(0, 0, ([UIScreen mainScreen].bounds.size.width ), ([UIScreen mainScreen].bounds.size.height-60.5));
        [webVw loadRequest:urlRequest];
    
        [self.view.window addSubview:webVw];
    
    bcview=[[UIView alloc]initWithFrame:CGRectMake(0, ([UIScreen mainScreen].bounds.size.height-60),([UIScreen mainScreen].bounds.size.width ), 60)];
    bcview.layer.backgroundColor=[UIColor colorWithRed:7.0/255.0 green:84.0/255.0 blue:78.0/255.0 alpha:1.0].CGColor;
 
    buttonClose = [[UIButton alloc]init];
    
    //[button setTitle:@"Back" forState:UIControlStateNormal];
    [buttonClose setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    buttonClose.frame = CGRectMake(self.view.frame.size.width/2-20,10, 40, 40);
    //buttonClose.layer.cornerRadius=15.0;
    // buttonClose.layer.backgroundColor=[UIColor colorWithRed:7.0/255.0 green:84.0/255.0 blue:78.0/255.0 alpha:1.0].CGColor;
    [buttonClose setBackgroundImage:[UIImage imageNamed:@"x.png"] forState:normal];
    [buttonClose addTarget:self action:@selector(remove:) forControlEvents:UIControlEventTouchUpInside];
    [bcview addSubview:buttonClose];
   [self.view.window addSubview:bcview];
    
    }






-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    [MBProgressHUD showHUDAddedTo:webVw animated:YES];

}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
   
   
    [MBProgressHUD hideHUDForView:webVw animated:YES];
    
}



    




- (void)remove:(id)sender
{
    [webVw removeFromSuperview];
    [webVw stopLoading];
    webVw.delegate = nil;
    [bcview removeFromSuperview];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
