//
//  HomeViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface HomeViewController : UITabBarController<UISearchControllerDelegate, UISearchResultsUpdating>

@end
