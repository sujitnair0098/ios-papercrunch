//
//  FeedCapsMenuViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 25/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "TrendingViewController.h"
#import "MyBookmarksViewController.h"
#import "FeedViewController.h"


@interface FeedCapsMenuViewController : UIViewController<UISearchControllerDelegate, UISearchResultsUpdating,UIScrollViewDelegate,UISearchBarDelegate>{

UISearchController *searchController;
}
@property(nonatomic,strong)CAPSPageMenu *pagemenu;

@end
