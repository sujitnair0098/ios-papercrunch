//
//  FeedCapsMenuViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 25/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "FeedCapsMenuViewController.h"

@interface FeedCapsMenuViewController ()

@end

@implementation FeedCapsMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = self;
    
    searchController.searchBar.placeholder = @"Ask Paper Crunch";
    searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchController.searchBar.barTintColor = [UIColor whiteColor];
    searchController.searchBar.tintColor = [UIColor whiteColor];
    searchController.delegate=self;
    searchController.searchBar.delegate=self;
    
    
    self.navigationItem.titleView = searchController.searchBar;
    self.definesPresentationContext = YES;
    
    FeedViewController *fedvc=[self.storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
     fedvc.homeNav = self.navigationController;
    [fedvc setTitle:@"Feed"];
    
    TrendingViewController *trendvc=[self.storyboard instantiateViewControllerWithIdentifier:@"Trending"];//[[MyTopicsViewController alloc]init];
    [ trendvc setTitle:@"Trending"];
    
    MyBookmarksViewController *mybookvc=[self.storyboard instantiateViewControllerWithIdentifier:@"MyBookmarks"];
    [mybookvc setTitle:@"Bookmarks"];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *controllerArray=@[fedvc,mybookvc,trendvc];
    
    _pagemenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:[self pageMenuDefaultPropertiesWithLineColor:[UIColor clearColor]]];
    //  pagemenu.delegate = self;
    [self.view addSubview:_pagemenu.view];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDictionary *)pageMenuDefaultPropertiesWithLineColor:(UIColor *)lineColor {
    return @{
             CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:7.0/255.0 green:84.0/255.0 blue:78.0/255.0 alpha:1.0],//ui update indigoPageMenuBackGroundColor //ui humm changed to back1
             CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
             CAPSPageMenuOptionSelectionIndicatorColor: lineColor,
             CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor lightGrayColor],//clear color
             CAPSPageMenuOptionMenuItemFont: [UIFont systemFontOfSize:18],
             CAPSPageMenuOptionMenuHeight: @(44.0),
             CAPSPageMenuOptionMenuItemWidth: @(90.0),
             CAPSPageMenuOptionCenterMenuItems: @(NO),
             CAPSPageMenuOptionUseMenuLikeSegmentedControl:@(YES),
             CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor whiteColor],             CAPSPageMenuOptionUnselectedMenuItemLabelColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0],//indigoVioletDark
             CAPSPageMenuOptionSelectionIndicatorHeight:@(4.0),//ui update 2.0
             };
    
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
