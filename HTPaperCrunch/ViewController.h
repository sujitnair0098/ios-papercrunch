//
//  ViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 16/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorViewController.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;

- (IBAction)didTapButton1:(id)sender;
- (IBAction)didTapButton2:(id)sender;



@end

