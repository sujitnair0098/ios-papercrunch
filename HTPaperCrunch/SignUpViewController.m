//
//  SignUpViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "SignUpViewController.h"
#import "HTChatListViewController.h"
#import "FeedCapsMenuViewController.h"
#import "PaperViewController.h"
#import "NotificationsViewController.h"
#import "ProfileViewController.h"


@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize scrollView;
@synthesize view_signUp;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIColor *color = [UIColor darkGrayColor];
    
//    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
//    self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+100);
//    self.scrollView.backgroundColor =[UIColor redColor];
//    self.scrollView.delegate = self;
//    self.scrollView.scrollEnabled = YES;
//    self.scrollView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0);
//    //self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
//    //[self.scrollView setContentOffset:CGPointMake(0, -100)];
//    [self.scrollView addSubview:self.view_signUp];
//    [self.view addSubview:self.scrollView];
    
//    _textFieldFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: color}];
//    _textFieldLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: color}];
//    _textFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: color}];
//    
//    _textFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
//    _textFieldConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];

    _imageView.layer.cornerRadius = self.imageView.frame.size.width / 2;
    _imageView.clipsToBounds = YES;
    
    _textFieldFirstName.delegate = self;
    _textFieldLastName.delegate = self;
    _textFieldPassword.delegate = self;
    _textFieldConfirmPassword.delegate = self;
    _textFieldEmail.delegate=self;
    
    _textFieldFirstName.returnKeyType = UIReturnKeyNext;
    _textFieldLastName.returnKeyType = UIReturnKeyNext;
    _textFieldEmail.returnKeyType=UIReturnKeyNext;
    _textFieldPassword.returnKeyType = UIReturnKeyNext;
    _textFieldConfirmPassword.returnKeyType = UIReturnKeyDone;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapImageButton:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;

    UIAlertController * AddImageActionSheet = [UIAlertController alertControllerWithTitle:@"Image Picker"                                                                                  message:@"Pick image from option below"                                                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Below code ll applicable for only iPads.{
    AddImageActionSheet.popoverPresentationController.sourceView = self.view;
    AddImageActionSheet.popoverPresentationController.sourceRect = _imageView.frame;
    //}
    
    UIAlertAction * camera = [UIAlertAction actionWithTitle:@"Take a pic" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                              {
                                  imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                  
                                  [self presentViewController:imagePickerController animated:YES completion:nil];
                                  //Presenting UIImagePickerContrloller i.e. imagePicker having SourceType Camera.
                              }];
    
    UIAlertAction * gallery = [UIAlertAction actionWithTitle:@"Gallery"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action)
                               {
                                   imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                   
                                   [self presentViewController:imagePickerController
                                                      animated:YES
                                                    completion:nil];
                                   //Presenting UIImagePickerContrloller i.e. imagePicker having SourceType Gallery(Photo Library).
                                   
                               }];
    
    [AddImageActionSheet addAction:camera];
    [AddImageActionSheet addAction:gallery];
    
    [self presentViewController:AddImageActionSheet animated:YES completion:nil];
    //Presenting UIAlertContrloller i.e. AddImageActionSheet
    
////////////////////////////////// OR //////////////////////////
//Its need third party library UIAlertControllerBlocks.
    
//    [UIAlertController showActionSheetInViewController:self
//                                             withTitle:@"Image Picker"
//                                               message:@"Pick image from action below"
//                                     cancelButtonTitle:nil
//                                destructiveButtonTitle:nil
//                                     otherButtonTitles:@[@"Camera",@"Gallery"]
//#if TARGET_OS_IOS
//                    popoverPresentationControllerBlock:^(UIPopoverPresentationController * popover)
//                    {
//                        popover.sourceView = self.view;
//                        popover.sourceRect = _imageView.frame;
//                    }
//#endif                                             
//                                              tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex)
//    {
//        if (buttonIndex == controller.firstOtherButtonIndex)
//        {
//            NSLog(@"Camera");
//            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
//            
//            [self presentViewController:imagePickerController animated:YES completion:nil];
//            //Presenting UIImagePickerContrloller i.e. imagePicker having SourceType Camera.
//            
//        }
//        else
//        {
//            NSLog(@"Gallery");
//            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//            
//            [self presentViewController:imagePickerController
//                               animated:YES
//                             completion:nil];
//            //Presenting UIImagePickerContrloller i.e. imagePicker having SourceType Gallery(Photo Library).
//
//        }
//    }];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)didTapSignUpButton:(id)sender
{
    if (_textFieldFirstName.text.length && _textFieldLastName.text.length && _textFieldPassword.text.length && _textFieldConfirmPassword.text.length != 0 && _textFieldEmail.text.length !=0)
    {
        
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        //Valid email address
        
        if ([emailTest evaluateWithObject:_textFieldEmail.text] == YES)
        {
            
            if (_textFieldPassword.text == _textFieldConfirmPassword.text)
            {
                //Code Here
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setValue:_textFieldFirstName.text forKey:@"firstname"];
                [dict setValue:_textFieldLastName.text forKey:@"lastname"];
                
                User *currentuser=[[User alloc]initWithDict:dict];
                [[SA_MANAGER sharedManager].arrUsers addObject:currentuser];
                
                
                [self postUserData];
            }
            else
            {
                alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Password Missmatch" preferredStyle:UIAlertControllerStyleAlert];
                
                //Below code ll applicable for only iPads.{
                alertController.popoverPresentationController.sourceView = self.view;
                alertController.popoverPresentationController.sourceRect = _buttonSignUp.frame;
                //}
                
                UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                      {
                                          [_textFieldPassword becomeFirstResponder];
                                          _textFieldPassword.text = @"";
                                          _textFieldConfirmPassword.text = @"";
                                          
                                      }];
                
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
        else{
        
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Please enter a valid email id" preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = _buttonSignUp.frame;
            //}
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      [_textFieldEmail becomeFirstResponder];
                                      _textFieldEmail.text = @"";
                                      
                                  }];
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        
        
        
        }
           
        }
        
            else
    {
        alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"All fields are mandatory." preferredStyle:UIAlertControllerStyleAlert];
        
        //Below code ll applicable for only iPads.{
        alertController.popoverPresentationController.sourceView = self.view;
        alertController.popoverPresentationController.sourceRect = _buttonSignUp.frame;
        //}
        
        UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];

    }
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    _imageViewPlaceholder.hidden = YES;
    UIImage * image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    if (image != nil)
    {
        _imageView.image = image;
        [_imageView setImage:image];
        _imageView.hidden = NO;
               
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    else
        NSLog(@"Image is nil");
    
    [picker dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField == _textFieldFirstName)
    {
        [_textFieldLastName becomeFirstResponder];
    }
    else if (textField == _textFieldLastName)
    {
        [_textFieldEmail becomeFirstResponder];
    }
    
    else if (textField==_textFieldEmail){
        [_textFieldPassword becomeFirstResponder];
    
    }
    
    else if (textField == _textFieldPassword)
    {
        [_textFieldConfirmPassword becomeFirstResponder];
    }
    
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    
    if (textField == _textFieldLastName)
    {
        NSRange whiteSpaceRange = [_textFieldFirstName.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        NSRange symbolRange = [_textFieldFirstName.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]];
        
        if ((whiteSpaceRange.location != NSNotFound)||(symbolRange.location != NSNotFound))
        {
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Name shouldn't contain blank spaces or Symbols." preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = _buttonSignUp.frame;
            //}
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];
            
        }
    }
    else if (textField == _textFieldPassword)
    {
        NSRange whiteSpaceRange = [_textFieldLastName.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (whiteSpaceRange.location != NSNotFound)
        {
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Last Name shouldn't contain blank spaces." preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = _buttonSignUp.frame;
            //}
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];
            
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    activeTextField = nil;
}

-(void)postUserData{

   // NSDictionary *userData = @{@"firstName":_textFieldFirstName.text,@"lastName":_textFieldLastName.text,@"password":_textFieldPassword.text,@"emailId":_textFieldEmail.text};
  //  NSString *userData =[NSString stringWithFormat:@"&firstName/%@&lastName/%@&emailId/%@&password%@",_textFieldFirstName.text,_textFieldLastName.text,_textFieldEmail.text,_textFieldPassword.text,nil];
    NSString *strToken=[SA_MANAGER sharedManager].deviceToken;
//    NSString *strToken = @"90c358dd573804e3b1cc13069244fd51997da2cfb9715efc7ff415fc1d7e6f36";
    
    NSString *strUrl=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_UserSignup/firstName/%@/lastName/%@/emailId/%@/password/%@/deviceToken/%@/deviceType/1",_textFieldFirstName.text,_textFieldLastName.text,_textFieldEmail.text,_textFieldPassword.text,strToken];
    
        NSLog(@"user data:%@", strUrl);
    
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
   
   NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:strUrl,@"rqBody", nil];
    
    
    
    
    NSError *error=nil;
    
   // NSData *postData=[NSJSONSerialization dataWithJSONObject:requestData options:kNilOptions error:&error];
    NSData *postData=[strUrl dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *jsonString = [[NSString alloc] initWithData:postData
                                                 encoding:NSUTF8StringEncoding];
    NSLog(@"Response JSON=%@", jsonString);
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    NSURLResponse *response;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[resSrt dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"%@",result);
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    User *currentuser=[[User alloc]initWithDict:dict];
    
    [dict setValue:[result valueForKey:@"userid"] forKey:@"userid"];
    [[SA_MANAGER sharedManager].arrUsers addObject:currentuser];
   
    
    NSString *codeR = [[result objectForKey:@"response"] stringValue];
    // mysterious missing code
    if ([codeR isEqualToString:@"1"])
        
    {
        if ([[result objectForKey:@"message"] isEqualToString:@"This emailId is already registered"]) {
            
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"This emailId is already registered." preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = _buttonSignUp.frame;
            //}
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];

            
        }
        else{
            HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
            [obj_chatListView setLoginUserId:[result objectForKey:@"userid"]];

        
        
        
           [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [self showMessage:@"Welcome to PaperCrunch" atPoint:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2)];
        
             HomeViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [self presentViewController:vc animated:YES completion:nil];
            
            
          /*  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
           
            
            UIStoryboard *storyBoard_chatSDK =[UIStoryboard storyboardWithName:@"ChatMain" bundle:[NSBundle mainBundle]];
            HTChatListViewController *obj_chatView =[storyBoard_chatSDK instantiateViewControllerWithIdentifier:@"HTChatListViewController"];

            HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
            [obj_chatListView setLoginUserId:[result objectForKey:@"userid"]];
            
            UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            FeedCapsMenuViewController *obj_feedView =[storyBoard instantiateViewControllerWithIdentifier:@"FeedCapsMenuViewController"];
            PaperViewController *obj_paperView = [storyBoard instantiateViewControllerWithIdentifier:@"PaperViewController"];
            NotificationsViewController *obj_notificationsView = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationsView"];
            ProfileViewController *obj_profileView = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            HomeViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            vc.viewControllers = @[obj_feedView,obj_paperView,obj_chatView,obj_notificationsView,obj_profileView
                                   ];
            //        [[vc.tabBar.items objectAtIndex:0] setBadgeValue:@"Feed"];
            //        [[vc.tabBar.items objectAtIndex:1] setBadgeValue:@"Papers"];
            [[vc.tabBar.items objectAtIndex:2] setTitle:@"Chat"];
            [[vc.tabBar.items objectAtIndex:2] setImage:[UIImage imageNamed:@"tab75-75chat"]];
            
            //        [[vc.tabBar.items objectAtIndex:3] setBadgeValue:@"Notifications"];
            //        [[vc.tabBar.items objectAtIndex:4] setBadgeValue:@"Profile"];
            //
            
            
            [self presentViewController:vc animated:YES completion:nil];*/
            

            
            
        
        }
    }
    else
    {
        NSLog(@"faield to connect");
    }
    
}

- (void)showMessage:(NSString*)message atPoint:(CGPoint)point {
    const CGFloat fontSize = 18;  // Or whatever.
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize];  // Or whatever.
    label.text = message;
    label.textColor = [UIColor greenColor];  // Or whatever.
    [label sizeToFit];
    
    label.center = point;
    
    [self.view addSubview:label];
    
    [UIView animateWithDuration:0.3 delay:1 options:0 animations:^{
        label.alpha = 0;
    } completion:^(BOOL finished) {
        label.hidden = YES;
        [label removeFromSuperview];
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
            [self.scrollView scrollRectToVisible:activeTextField.frame animated:YES];
        }
    }];
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

@end
