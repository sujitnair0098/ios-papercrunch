//
//  FeedViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "FeedViewController.h"

@interface FeedViewController ()

@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        
   // [self showMessage:@"Welcome to PaperCrunch" atPoint:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2)];

    
    // Do any additional setup after loading the view.
    [_labelFeedSegment setTitleColor:[UIColor whiteColor] forState:normal];
   // [_imageViewFeedSegment setImage:[UIImage imageNamed:@"feed.png"]];
    
     _scrollView.delegate = self;
    
  [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    
    
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = self;
    
    searchController.searchBar.placeholder = @"Ask Paper Crunch";
    searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchController.searchBar.barTintColor = [UIColor whiteColor];
    searchController.searchBar.tintColor = [UIColor whiteColor];
    searchController.delegate=self;
    searchController.searchBar.delegate=self;

    
    self.navigationItem.titleView = searchController.searchBar;
    self.definesPresentationContext = YES;
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //To disable vertical scrolling
    [scrollView setContentOffset: CGPointMake(scrollView.contentOffset.x, oldY)];
    
}

- (void)showMessage:(NSString*)message atPoint:(CGPoint)point {
    const CGFloat fontSize = 24;  // Or whatever.
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor blueColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize];  // Or whatever.
    label.text = message;
    label.textColor = [UIColor blueColor];  // Or whatever.
    [label sizeToFit];
    
    label.center = point;
    
    [self.view addSubview:label];
    
    [UIView animateWithDuration:0.8 delay:2 options:0 animations:^{
        label.alpha = 0;
    } completion:^(BOOL finished) {
        label.hidden = YES;
        [label removeFromSuperview];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didChangeValue:(UISegmentedControl *)sender
{
    switch ([sender selectedSegmentIndex]) {
        case 0:
            _label.text = @"Feed";
            break;
        case 1:
            _label.text = @"Bookmarks";
            break;
        default:
            _label.text = @"Trending";
            break;

    
}

//    if (sender.selectedSegmentIndex == 0) {
//        [_label setText:@"My Papers"];
//    }
//    else if (sender.selectedSegmentIndex == 1) {
//        [_label setText:@"Backlogs"];
//    }
//    else if (sender.selectedSegmentIndex == 2) {
//        [_label setText:@"Trending"];
//    }
}
- (IBAction)didTapSettingButton:(id)sender {
    
    SettingsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)didTapFeedSegmentButton:(id)sender
{
    [_labelFeedSegment setTitleColor:[UIColor whiteColor] forState:normal];
    
    [_labelBookmarkSegment setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [_labelTrendingSegment setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    //    [_imageViewFeedSegment setImage:[UIImage imageNamed:@"feed.png"]];
//    [_imageViewTrendingSegment setImage:[UIImage imageNamed:@"trendygrn.png"]];
//    [_imageViewBookmarkSegment setImage:[UIImage imageNamed:@"bookmkgrn.png"]];
}

- (IBAction)didTapTrendingSegmentButton:(id)sender
{
    [_labelTrendingSegment setTitleColor:[UIColor whiteColor] forState:normal];
    
    [_labelBookmarkSegment setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [_labelFeedSegment setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
     
     //    [_imageViewFeedSegment setImage:[UIImage imageNamed:@"feedgrn.png"]];
//    [_imageViewTrendingSegment setImage:[UIImage imageNamed:@"trendy.png"]];
//    [_imageViewBookmarkSegment setImage:[UIImage imageNamed:@"bookmkgrn.png"]];
    
}

- (IBAction)didTapBookmarkSegmentButton:(id)sender
{
    [_labelBookmarkSegment setTitleColor:[UIColor whiteColor] forState:normal];
    
    [_labelTrendingSegment setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [_labelFeedSegment setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//    [_imageViewFeedSegment setImage:[UIImage imageNamed:@"feedgrn.png"]];
//    [_imageViewTrendingSegment setImage:[UIImage imageNamed:@"trendygrn.png"]];
//    [_imageViewBookmarkSegment setImage:[UIImage imageNamed:@"bookmk.png"]];
    

}
@end
