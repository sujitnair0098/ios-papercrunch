//
//  RefineViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 24/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "RefineViewController.h"

@interface RefineViewController ()

@end

@implementation RefineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _arrBranches=[[NSMutableArray alloc]init];
    
    arrRefineCateogory=[[NSMutableArray alloc]initWithObjects:@"All",@"By branch",@"By semister",@"By subject",@"By year", nil];
    
    
    [_segFilterSort setSelectedSegmentIndex:0];
    
    [_segFilterSort setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    
    
    [_segFilterSort setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:37.0/255.0 green:90.0/255.0 blue:84.0/255.0 alpha:1.0] } forState:UIControlStateNormal];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.backgroundColor = [[UIColor colorWithRed:37.0/255.0 green:90.0/255.0 blue:84.0/255.0 alpha:1.0] CGColor];
    bottomBorder.frame = CGRectMake(0, 65, CGRectGetWidth(self.vw.frame), 1.0);
    [self.vw.layer addSublayer:bottomBorder];
    
    
    UIButton *btntitle=[UIButton buttonWithType:UIButtonTypeCustom];
    btntitle.frame=CGRectMake(125, 0, 125, 30);
    [btntitle setTitle:@"Refine list" forState:normal];
    //btntitle.titleLabel.font= [UIFont fontWithDescriptor:[UIFontDescriptor fontDescriptorWithFontAttributes:@{}] size:<#(CGFloat)#>
    btntitle.titleLabel.font=[UIFont systemFontOfSize:20.0 weight:0.1];
    [btntitle setTitleColor:[UIColor colorWithRed:194.0/255.0 green:239.0/255.0 blue:231.0/255.0 alpha:1.0] forState:normal];
    [btntitle setEnabled:NO];
    [btntitle sizeToFit];
 self.navigationItem.titleView =btntitle;
    
    self.navigationItem.titleView.frame=CGRectMake(125, 0, 100, 30);
   


    
    
    UIButton *btnCancels=[UIButton buttonWithType:UIButtonTypeCustom];
                        btnCancels.frame=CGRectMake(0, 0, 125, 30);
    [btnCancels setTitle:@"Cancel" forState:normal];
    btnCancels.titleLabel.font= [UIFont systemFontOfSize:20.0 weight:0.1];
    [btnCancels setTitleColor:[UIColor colorWithRed:194.0/255.0 green:239.0/255.0 blue:231.0/255.0 alpha:1.0] forState:normal];
    [btnCancels sizeToFit];
    [btnCancels addTarget:self action:@selector(btnCancelClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *lbarBtn = [[UIBarButtonItem alloc] initWithCustomView:btnCancels];
    self.navigationItem.leftBarButtonItem=lbarBtn;

    UIButton *btnApplies=[UIButton buttonWithType:UIButtonTypeCustom];
    btnApplies.frame=CGRectMake(250, 0, 125, 30);
    [btnApplies setTitle:@"Apply" forState:normal];
    btnApplies.titleLabel.font= [UIFont systemFontOfSize:20.0 weight:0.1];
    [btnApplies setTitleColor:[UIColor colorWithRed:194.0/255.0 green:239.0/255.0 blue:231.0/255.0 alpha:1.0] forState:normal];
    [btnApplies sizeToFit];
    //[btnApplies addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rbarBtn = [[UIBarButtonItem alloc] initWithCustomView:btnApplies];
    self.navigationItem.rightBarButtonItem=rbarBtn;
    
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrRefineCateogory.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSString *cellid=@"cellid";
    
    UITableViewCell *cell=[_tblRefine dequeueReusableCellWithIdentifier:cellid];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellid"];
    }
    
    cell.textLabel.text=[arrRefineCateogory objectAtIndex:indexPath.row];
    
//    UIButton *yourButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    yourButton.frame = CGRectMake(0, 0, 25, 25);
    
    
    if (indexPath.row==0) {
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    else {
        cell.accessoryView=[[ UIImageView alloc ]
                            initWithImage:[UIImage imageNamed:@"accessoryarrow"]];

    }
    
        
    return cell;
  

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *currentCell=[_tblRefine cellForRowAtIndexPath:indexPath];
    
    if ([currentCell.textLabel.text isEqualToString:@"All"]) {
        if (currentCell.accessoryView==nil) {
            currentCell.accessoryView=[[ UIImageView alloc ]
                                       initWithImage:[UIImage imageNamed:@"accessorytickmark"]];
        }
        else{
            currentCell.accessoryView=nil;
        }
    }
    
    else{
        
        [self performSegueWithIdentifier:@"Branch" sender:nil];
              
        
  //     if ([currentCell.textLabel.text isEqualToString:@"By branch"]) {
           
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
   // BranchViewController *myNVC =[storyboard instantiateViewControllerWithIdentifier:@"Branches"];
  //  myNVC.arrBranch=[[NSMutableArray alloc]initWithObjects:@"one",@"two", nil];
  //  [self.navigationController pushViewController:myNVC animated:YES];
           
          
      //  }
    }
    

}



-(void)btnCancelClicked{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    }


-(void)viewWillAppear:(BOOL)animated{

    self.navigationItem.hidesBackButton = YES;
    
   
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
        if ([segue.identifier isEqualToString:@"Branch"]) {
        
        BranchViewController *brVC=[segue destinationViewController];
        
}

}
        




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
