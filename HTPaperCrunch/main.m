//
//  main.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 16/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
