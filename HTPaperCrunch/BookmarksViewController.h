//
//  BookmarksViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 22/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyPapersViewController.h"


@interface BookmarksViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,Addbookmark>
{
    NSMutableArray *arrCell;
}

@property(nonatomic,weak) IBOutlet UITableView *tblBookmarks;

@end
