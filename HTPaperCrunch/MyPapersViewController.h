//
//  MyPapersViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondViewController.h"
#import "MyPaperTableViewCell.h"
#import "PaperViewController.h"
#import "MBProgressHUD.h"


@protocol Addbookmark <NSObject>

-(void)Passcell:(UITableViewCell *)cell;

@end

@interface MyPapersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>{
    NSMutableDictionary *dictPapers;
    NSMutableArray *arrPapers;
    NSMutableArray *arrExam;
    UIWebView *webVw;
    UIBarButtonItem *backButton;
    UIView *bcview;
}
@property(nonatomic,strong) id<Addbookmark>delegate;
@property(nonatomic,weak) IBOutlet UITableView *tblPapers;
@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;
@end
