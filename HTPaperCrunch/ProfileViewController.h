//
//  ProfileViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 13/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iCarousel/iCarousel.h>
#import "SA_MANAGER.h"
#import "User.h"
#import "FBSDKLoginKit.h"
#import "FBSDKCoreKit.h"
#import "SignUpViewController.h"
#import "CAPSPageMenu.h"
#import "SatisticsViewController.h"
#import "NotificationsViewController.h"

@interface ProfileViewController : UIViewController<UISearchControllerDelegate, UISearchResultsUpdating,iCarouselDataSource, iCarouselDelegate,UIScrollViewDelegate>
{
    float oldY;
    UISearchController *searchController;
}

@property (strong, nonatomic) IBOutlet iCarousel *carousel;

@property (nonatomic, strong) NSMutableArray *items;


@property (strong, nonatomic) IBOutlet UIView *viewFirst;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property(nonatomic,weak) IBOutlet UIButton *btnProfile;
@property(nonatomic,weak) IBOutlet UIButton *btnSattistics;
@property(nonatomic,weak) IBOutlet UIButton *btnNotifs;
@property(nonatomic,weak) IBOutlet UILabel *lblName;
@property(nonatomic,weak) IBOutlet UILabel *lblCollege;
@property(nonatomic,weak) IBOutlet UILabel *lblSchool;
@property(nonatomic,weak) IBOutlet UIImageView *imgProfile;
@property(nonatomic,weak) IBOutlet UILabel *lblArea;
@property(nonatomic,strong) IBOutlet UIView *vwAbout;
@property(nonatomic,strong) NSMutableDictionary *userDict;

@property(nonatomic,strong) NSString *strName;
@property(nonatomic,copy) NSString *strCollege;
@property(nonatomic,copy) NSString *strSchool;
@property(nonatomic,copy) NSString *strArea;
@property(nonatomic,copy) UIImage *imgProfilePic;

@property(nonatomic,strong) UINavigationController* homeNav;
-(IBAction)btnProfileClicked:(id)sender;
-(IBAction)btnStatisticsClicked:(id)sender;
-(IBAction)btnNotifsClicked:(id)sender;
@end
