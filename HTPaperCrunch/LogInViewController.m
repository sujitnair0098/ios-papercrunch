//
//  LogInViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "LogInViewController.h"
#import "HTChatListViewController.h"
#import "FeedCapsMenuViewController.h"
#import "PaperViewController.h"
#import "NotificationsViewController.h"
#import "ProfileViewController.h"
@interface LogInViewController ()

@end

@implementation LogInViewController
@synthesize scrollView_login;
@synthesize view_login;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIColor *color = [UIColor darkGrayColor];    
    // Do any additional setup after loading the view.
    _txtEmailId.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: color}];
    
    _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    _txtEmailId.delegate = self;
    _txtPassword.delegate = self;
    
    _txtEmailId.returnKeyType = UIReturnKeyNext;
    _txtPassword.returnKeyType = UIReturnKeyDone;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    
    if (textField == _txtEmailId)
    {
        NSRange whiteSpaceRange = [_txtEmailId.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if ((whiteSpaceRange.location != NSNotFound))
        {
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Name shouldn't contain blank spaces or Symbols." preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = btnLogin.frame;
            //}
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];
            
        }
    }
    else if (textField == _txtPassword)
    {
        NSRange whiteSpaceRange = [_txtPassword.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (whiteSpaceRange.location != NSNotFound)
        {
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Last Name shouldn't contain blank spaces." preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = btnLogin.frame;
            //}
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];
            
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{    activeTextField = nil;
    
    if (textField == _txtEmailId)
    {
        [_txtPassword becomeFirstResponder];
    }
    
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(IBAction)btnSigninClicked:(id)sender{


    if (_txtEmailId.text.length != 0 && _txtPassword.text.length !=0) {
        
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        //Valid email address
        
        if ([emailTest evaluateWithObject:_txtEmailId.text] == YES)
        {
             [self Login];
        }
        else
        {
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Please enter a valid email id" preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = btnLogin.frame;
            //}
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      [_txtEmailId becomeFirstResponder];
                                      _txtEmailId.text = @"";
                                 
                                  }];
            
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            

        }
        
    }

    else{
    
        alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Fields cannot empty" preferredStyle:UIAlertControllerStyleAlert];
        
        //Below code ll applicable for only iPads.{
        alertController.popoverPresentationController.sourceView = self.view;
        alertController.popoverPresentationController.sourceRect = btnLogin.frame;
        //}
        
        UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                              {
                                  [_txtEmailId becomeFirstResponder];
                                  _txtEmailId.text = @"";
                                  _txtPassword.text = @"";
                                  
                              }];
        
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    
    }

-(IBAction)btnSignupClicked:(id)sender{

    SignUpViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self presentViewController:vc animated:YES completion:nil];


}


-(void)Login{

    NSString *strToken=[SA_MANAGER sharedManager].deviceToken;
//    NSString *strToken = @"90c358dd573804e3b1cc13069244fd51997da2cfb9715efc7ff415fc1d7e6f36";
    
    NSString *strUrl=[NSString stringWithFormat:@"http://httest.in//papercrunchws/RUC_UserLogin/emailId/%@/password/%@/deviceToken/%@/deviceType/1",_txtEmailId.text,_txtPassword.text,strToken];
    
    NSLog(@"user data:%@", strUrl);
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:strUrl,@"rqBody", nil];
    
    
    
    
    NSError *error=nil;
    
    // NSData *postData=[NSJSONSerialization dataWithJSONObject:requestData options:kNilOptions error:&error];
    NSData *postData=[strUrl dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *jsonString = [[NSString alloc] initWithData:postData
                                                 encoding:NSUTF8StringEncoding];
    NSLog(@"Response JSON=%@", jsonString);
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    NSURLResponse *response;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[resSrt dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"%@",result);
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[result objectForKey:@"firstName"] forKey:@"firstName"];
    [dict setValue:[result objectForKey:@"lastName"] forKey:@"lastName"];
    [dict setValue:[result objectForKey:@"userid"] forKey:@"userId"];
    
    User *currentuser=[[User alloc]initWithDict:dict];
    [[SA_MANAGER sharedManager].arrUsers addObject:currentuser];
 
    NSString *codeR = [[result objectForKey:@"response"] stringValue];
    // mysterious missing code
    if ([codeR isEqualToString:@"1"])
        
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
        [obj_chatListView setLoginUserId:[result objectForKey:@"userid"]];
        
        
        
       /* UIStoryboard *storyBoard_chatSDK =[UIStoryboard storyboardWithName:@"ChatMain" bundle:[NSBundle mainBundle]];
        HTChatListViewController *obj_chatView =[storyBoard_chatSDK instantiateViewControllerWithIdentifier:@"HTChatListViewController"];
        HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
        [obj_chatListView setLoginUserId:[result objectForKey:@"userid"]];
        
        UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        FeedCapsMenuViewController *obj_feedView =[storyBoard instantiateViewControllerWithIdentifier:@"FeedCapsMenuViewController"];
        PaperViewController *obj_paperView = [storyBoard instantiateViewControllerWithIdentifier:@"PaperViewController"];
        NotificationsViewController *obj_notificationsView = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationsView"];
        ProfileViewController *obj_profileView = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        
        HomeViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        vc.viewControllers = @[obj_feedView,obj_paperView,obj_chatView,obj_notificationsView,obj_profileView
                               ];
//        [[vc.tabBar.items objectAtIndex:0] setBadgeValue:@"Feed"];
//        [[vc.tabBar.items objectAtIndex:1] setBadgeValue:@"Papers"];
        [[vc.tabBar.items objectAtIndex:2] setTitle:@"Chat"];
        [[vc.tabBar.items objectAtIndex:2] setImage:[UIImage imageNamed:@"tab75-75chat"]];

//        [[vc.tabBar.items objectAtIndex:3] setBadgeValue:@"Notifications"];
//        [[vc.tabBar.items objectAtIndex:4] setBadgeValue:@"Profile"];*/
//
        HomeViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];

        
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    else
    {
        NSLog(@"failed to connect");
        
        alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Invalid username or password" preferredStyle:UIAlertControllerStyleAlert];
        
        //Below code ll applicable for only iPads.{
        alertController.popoverPresentationController.sourceView = self.view;
        alertController.popoverPresentationController.sourceRect = btnLogin.frame;
        //}
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:ok];
    }



}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        self.scrollView_login.contentInset = contentInsets;
        self.scrollView_login.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
            [self.scrollView_login scrollRectToVisible:activeTextField.frame animated:YES];
        }
    }];
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView_login.contentInset = contentInsets;
    self.scrollView_login.scrollIndicatorInsets = contentInsets;
}


@end
