//
//  WebServiceHandler.h
//  HTWebServiceManager
//
//  Created by HelixTech-Admin on 30/11/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum Int {
    eHTWebserviceResponseTypeJSON,
    eHTWebserviceResponseTypeXML
    
}HTWebserviceResponseType;


@interface HTWebServiceHandler : NSObject{
    
}

+ (HTWebServiceHandler *)sharedInstance;

typedef void (^WebServiceHandlerSuccessBlock)(id result);
typedef void (^WebServiceHandlerFailureBlock)(id result);



- (instancetype)init __attribute__((unavailable("You cannot init this class directly, use sharedInstance")));
+ (instancetype)new __attribute__((unavailable("You cannot call new for this class directly, use sharedInstance")));

- (void)requestWithGetAndURLString:(NSString*)passUrlString andParameters:(NSDictionary *)parametersDictionary andWantReplaceForwarSlash:(BOOL)replaceSlash withResponseType:(HTWebserviceResponseType)responseType withSuccessBlock:(WebServiceHandlerSuccessBlock)success failure:(WebServiceHandlerFailureBlock)failure;

- (void)requestWithPostAndURLString:(NSString *)passUrlString andParameters:(NSDictionary *)parametersDictionary andWantReplaceForwarSlash:(BOOL)replaceSlash wantToAttachFilesToBody:(BOOL)isFilesNeedToAttach withNameToFile:(NSString *)nameOfFile withResponseType:(HTWebserviceResponseType)responseType andDataToPost:(NSData *)dataToPost withSuccessBlock:(WebServiceHandlerSuccessBlock)success failure:( WebServiceHandlerFailureBlock)failure;

- (void)dummyMethodToTestBlocks:(NSString *)data withSuccessBlock:(WebServiceHandlerSuccessBlock)success;


@end
