//
//  HTChatViewController.m
//  HTChat
//
//  Created by HelixTech-Admin  on 01/12/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import "HTChatViewController.h"
#import "HTChatListViewController.h"
#import "ChatViewCell.h"
#import "HTChatViewCell.h"
@interface HTChatViewController ()
{
//    ChatViewCell *cell;
}
@end

static CGFloat textViewAndSendButtonBottomSpacingConstant = 17;


@implementation HTChatViewController
static NSString * const reuseIdentifier = @"Cell";

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@synthesize tableView_Chat;
@synthesize txt_message;
@synthesize collectionView_chat;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString([[NSUserDefaults standardUserDefaults] valueForKey:@"USERNAME"], [[NSUserDefaults standardUserDefaults] valueForKey:@"USERNAME"]);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateChatView:) name:@"MESSAGERECEIVED" object:nil];
    
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x0A524E);
    self.sendButton.userInteractionEnabled = NO;
    [self.sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    
    collectionView_chat.alwaysBounceVertical = YES;
    
    [collectionView_chat registerClass:[HTChatViewCell class] forCellWithReuseIdentifier:reuseIdentifier];

    txt_message.layer.cornerRadius = 4.0;
    txt_message.layer.masksToBounds = YES;

    txt_message.delegate = self;
    [self fetchUserChatData];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture)];
    [self.collectionView_chat addGestureRecognizer:tapGesture];
    
}

- (void)handleTapGesture {
    [txt_message resignFirstResponder];
}

//- (void)viewWillLayoutSubviews {
//    [self upDatingCollectionViewShowPosition];
//}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView endEditing:YES];
    [textView resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ISCHATVIEW"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


-(void)keyboardWillShow:(NSNotification*)notification {
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    self.textViewBottomSpacingContraint.constant = keyboardFrameBeginRect.size.height - 30;
    self.sendButtonBottomSpacingConstraint.constant = keyboardFrameBeginRect.size.height - 30;
    
    self.collectionView_chat.contentOffset = CGPointMake(0, self.collectionView_chat.contentSize.height);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.view layoutIfNeeded];
        
        
//        self.collectionView_chat.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrameBeginRect.size.height, 0);
//        self.collectionView_chat.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, keyboardFrameBeginRect.size.height, 0);
    }];
 
    
}

-(void)keyboardWillHide:(NSNotification*)notification {
    
    self.textViewBottomSpacingContraint.constant = textViewAndSendButtonBottomSpacingConstant;
    self.sendButtonBottomSpacingConstraint.constant = textViewAndSendButtonBottomSpacingConstant;
    [UIView animateWithDuration:0.3 animations:^{
      
        [self.view layoutIfNeeded];
        
//        self.collectionView_chat.contentInset = UIEdgeInsetsZero;
//        self.collectionView_chat.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger num_rows = 0;
    if ([idarray count] > 0) {
        num_rows =[idarray count];
        return num_rows;
    }
    return num_rows;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HTChatViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor darkGrayColor],
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:8]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[[idarray objectAtIndex:indexPath.row] valueForKey:@"message"] ,[[idarray objectAtIndex:indexPath.row]valueForKey:@"sent_date"]] attributes:attribs];
    
    
    NSRange range = [[NSString stringWithFormat:@"%@\n\n%@",[[idarray objectAtIndex:indexPath.row] valueForKey:@"message"] ,[[idarray objectAtIndex:indexPath.row]valueForKey:@"sent_date"]] rangeOfString:[[idarray objectAtIndex:indexPath.row] valueForKey:@"message"]];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                    NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18]
                                    } range:range];
    
    NSString *textString = attributedText.string;
    

    
    
    CGFloat widthAnchorConstant = [self estimateHeightAndWidthOfText:textString].size.width + 30;
    cell.messageTextView.text = textString;

    if (![[[idarray objectAtIndex:indexPath.row]valueForKey:@"receiver_id"] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"]]) {

        cell.bubbleView.backgroundColor = [UIColor colorWithRed:0/255.0 green:137/255.0 blue:249/255.0 alpha:1];
        cell.messageTextView.textColor = [UIColor whiteColor];
        cell.bubbleViewLeftAnchor.active = NO;
        cell.bubbleViewRightAnchor.active = YES;
    } else {
        cell.bubbleView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        cell.messageTextView.textColor = [UIColor blackColor];
        cell.bubbleViewLeftAnchor.active = YES;
        cell.bubbleViewRightAnchor.active = NO;
    }
    
    cell.bubbleViewWidthAnchor.constant = widthAnchorConstant;
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 80;
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor darkGrayColor],
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:8]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[[idarray objectAtIndex:indexPath.row] valueForKey:@"message"] ,[[idarray objectAtIndex:indexPath.row]valueForKey:@"sent_date"]] attributes:attribs];
    
    
    NSRange range = [[NSString stringWithFormat:@"%@\n\n%@",[[idarray objectAtIndex:indexPath.row] valueForKey:@"message"] ,[[idarray objectAtIndex:indexPath.row]valueForKey:@"sent_date"]] rangeOfString:[[idarray objectAtIndex:indexPath.row] valueForKey:@"message"]];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                    NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18]
                                    } range:range];

    NSString *textString = attributedText.string;
    
    
    height = [self estimateHeightAndWidthOfText:textString].size.height + 20;
    
    return CGSizeMake(self.view.frame.size.width, height);
}



- (CGRect)estimateHeightAndWidthOfText: (NSString *) text {
    CGSize size = CGSizeMake(200, 1000);
    NSStringDrawingOptions options = (NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin);
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont systemFontOfSize:16]};
    
    return [[NSString stringWithString:text] boundingRectWithSize:size options:options attributes:attributes context:nil];
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ISCHATVIEW"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)updateChatView:(NSNotification *)notification{
    
    NSString *strUrl=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_SingleMessageDetails/message_id/%@",notification.object];
    
    NSLog(@"user data:%@", strUrl);
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:strUrl,@"rqBody", nil];
    
    NSError *error=nil;
    
    // NSData *postData=[NSJSONSerialization dataWithJSONObject:requestData options:kNilOptions error:&error];
    NSData *postData=[strUrl dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *jsonString = [[NSString alloc] initWithData:postData
                                                 encoding:NSUTF8StringEncoding];
    NSLog(@"Response JSON=%@", jsonString);
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        dataServer=[[NSMutableData alloc]init];
    }
    
}
-(void)addNewCell{
     [collectionView_chat performBatchUpdates:^{
         NSInteger resultsSize = [idarray count]-1; //data is the previous array of data
         
         NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:resultsSize
                                                               inSection:0]];
        [collectionView_chat insertItemsAtIndexPaths:arrayWithIndexPaths];
         
     } completion:^(BOOL finished) {
         if (finished) {
             [self upDatingCollectionViewShowPosition];
         }
     }];
    
    

    
 }
         
         
-(void)reloadChatView{
    [self fetchUserChatData];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}


-(void)setUserId:(NSString *)userId{
    NSLog(@"user %@",userId);
    str_userId =[[NSString alloc] initWithString:userId];
}
-(void)fetchUserChatData{
    
   // NSString *logedinuser=[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"userId"];
    //    NSString *urlString=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_GetUserMessages/loggedin_user_id/%@/other_user_id/%d",logedinuser,300];
    NSLog(@"user id: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTEDUSERID"]);
    NSString *urlString=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_GetUserMessages/loggedin_user_id/%@/other_user_id/%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTEDUSERID"]];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    NSURL *urlObj=[[NSURL alloc]initWithString:urlString];
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (IBAction)onClickSendMessage:(id)sender {
    [self sendmessages];
}

-(void)sendmessages{
    
    
//    NSString *myString = [_touser stringValue];
//    NSString *fromuser =[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"userId"];
    //        NSString *strUrl=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_SendMessage/sender_id/%@/receiver_id/%d/message/%@",fromuser ,300,_txtToSend.text];
    
    NSLog(@"user id: %@",str_userId);
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    NSString *textToSend = [NSString stringWithFormat:@"%@",txt_message.text];
    
//    textToSend = [textToSend stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    textToSend = [textToSend stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *strUrl=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_SendMessage/sender_id/%@/receiver_id/%@/message/%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"] ,[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTEDUSERID"],textToSend];
    
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    NSLog(@"user data:%@", strUrl);
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:strUrl,@"rqBody", nil];
    
    
    NSError *error=nil;
    
    // NSData *postData=[NSJSONSerialization dataWithJSONObject:requestData options:kNilOptions error:&error];
    NSData *postData=[strUrl dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *jsonString = [[NSString alloc] initWithData:postData
                                                 encoding:NSUTF8StringEncoding];
    NSLog(@"Response JSON=%@", jsonString);
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
//     NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    if (connection) {
//        dataServer=[[NSMutableData alloc]init];
//    }
    
    
    NSURLResponse *response;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    
    NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[resSrt dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"%@",result);
    
    
    NSString *codeR = [[result objectForKey:@"response"] stringValue];
    // mysterious missing code
    if ([codeR isEqualToString:@"1"])
        
    {
        
//        NSLog(@"sent");
//
//        [collectionView_chat reloadData];
//        [self upDatingCollectionViewShowPosition];
        
        txt_message.text = @"";
//        self.sendButton.userInteractionEnabled = NO;
        
        [self fetchUserChatData];
        
    }
    else
    {
        
        NSLog(@"failed to connect");
        
        alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Sorry could not establish connection to server" preferredStyle:UIAlertControllerStyleAlert];
        
        //Below code ll applicable for only iPads.{
        alertController.popoverPresentationController.sourceView = self.view;
        alertController.popoverPresentationController.sourceRect = txt_message.frame;
        //}
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:ok];
        
        
        
    }
    
}

- (void)upDatingCollectionViewShowPosition {

    if (idarray.count > 0) {
        NSInteger section = [collectionView_chat numberOfSections] - 1;
        NSInteger item = [collectionView_chat numberOfItemsInSection:section] - 1;
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
        [collectionView_chat scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionBottom) animated:YES];
    }
    
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [dataServer setLength:0];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    NSString *strresp=[[response objectForKey:@"response"]stringValue];
    
    if ([strresp isEqualToString:@"0"]) {
        
        NSLog(@"No records found");
    }
    
    else {
        if (idarray.count==0) {
            idarray =[[NSMutableArray alloc] init];
            
            [idarray addObjectsFromArray:[response objectForKey:@"data"]];
            for(int i = 0; i < idarray.count; i++) {
                if([[(NSString *) [idarray objectAtIndex:i] valueForKey:@"data"] isEqualToString:@"No records found in booking master"]){
                    
                    NSLog(@"no messages found");
                }
                
                else{
                    
                    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                    
                    //  Message *msg= [[Message alloc]initWithDict:dict];
                    
                    [dict setValue:[[idarray objectAtIndex:i] valueForKey:@"message"] forKey:@"strMessage"];
                    [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"message_id"] forKey:@"strMessageid"];
                    [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"receiver_id"] forKey:@"strReceiverid"];
                    [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"sender_id"] forKey:@"strSenderid"];
                    [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"sent_date"] forKey:@"strSentdate"];
                    
                    //  [[SA_MANAGER sharedManager].arrMessagedata addObject:msg];
                    
                    [collectionView_chat reloadData];
                    [self upDatingCollectionViewShowPosition];
                }
            }
        }
        else{
            if ([[response objectForKey:@"message"] isEqualToString:@"Success"]) {
                NSArray *arr_chatData =[response objectForKey:@"data"];
                if (arr_chatData.count==1) {
                    [idarray addObjectsFromArray:arr_chatData];
                    [self addNewCell];
                }
                else{
                    idarray =[[NSMutableArray alloc] init];
                    [idarray addObjectsFromArray:arr_chatData];
                    [collectionView_chat reloadData];
                    [self upDatingCollectionViewShowPosition];
                }
            }
          /*  else if ([[response objectForKey:@"message"] isEqualToString:@"Message sent successfully"]){
                NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *strDate =[dateFormatter stringFromDate:[NSDate date]];
                
                NSDictionary *dict =[[NSDictionary alloc] initWithObjectsAndKeys:txt_message.text,@"message",strDate,@"sent_date", nil];
                txt_message.text=@"";
                [self addNewCell:dict];
               
            }*/

        }
    }
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];

}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if (textView.text.length > 0) {
        self.sendButton.userInteractionEnabled = YES;
        [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        self.sendButton.userInteractionEnabled = NO;
        [self.sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}



@end
