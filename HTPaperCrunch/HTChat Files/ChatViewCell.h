//
//  ChatViewCell.h
//  HTChat
//
//  Created by HelixTech-Admin  on 02/12/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_receivedMessage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sentMessage;

@end
