//
//  ChatViewCell.m
//  ChatBubbles
//
//  Created by HelixTech-Admin on 06/12/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "HTChatViewCell.h"

@implementation HTChatViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        
        self.bubbleView = [[UIView alloc] init];
        self.bubbleView.translatesAutoresizingMaskIntoConstraints = false;
//        self.bubbleView.backgroundColor = [UIColor colorWithRed:0/255.0 green:137/255.0 blue:249/255.0 alpha:1];
        self.bubbleView.layer.cornerRadius = 16;
        self.bubbleView.layer.masksToBounds = YES;
        [self addSubview:self.bubbleView];
        
        
        self.messageTextView = [[UITextView alloc] init];
        self.messageTextView.translatesAutoresizingMaskIntoConstraints = false;
        self.messageTextView.backgroundColor = [UIColor clearColor];
        self.messageTextView.textColor = [UIColor whiteColor];
        self.messageTextView.font = [UIFont systemFontOfSize:16];
        self.messageTextView.editable = NO;
        self.messageTextView.userInteractionEnabled = NO;
        [self addSubview:self.messageTextView];
        
        //Constraints For Bubble View
//        [self.bubbleView.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-8].active = YES;
        self.bubbleViewRightAnchor = [self.bubbleView.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-8];
        self.bubbleViewRightAnchor.active = YES;
        
        self.bubbleViewLeftAnchor = [self.bubbleView.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:8];
        self.bubbleViewLeftAnchor.active = YES;
        
        
        [self.bubbleView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        self.bubbleViewWidthAnchor = [self.bubbleView.widthAnchor constraintEqualToConstant:200];
        self.bubbleViewWidthAnchor.active = YES;
        [self.bubbleView.heightAnchor constraintEqualToAnchor:self.heightAnchor].active = YES;
        
        //Constraints For Message View
        
        [self.messageTextView.leftAnchor constraintEqualToAnchor:self.bubbleView.leftAnchor constant:8].active = YES;
        [self.messageTextView.rightAnchor constraintEqualToAnchor:self.bubbleView.rightAnchor].active = YES;
        [self.messageTextView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [self.messageTextView.heightAnchor constraintEqualToAnchor:self.heightAnchor].active = YES;
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}


@end
