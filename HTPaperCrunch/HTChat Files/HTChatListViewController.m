//
//  HTChatListViewController.m
//  HTChat
//
//  Created by HelixTech-Admin  on 01/12/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import "HTChatListViewController.h"
#import "ChatListCell.h"
#import "HTChatViewController.h"
#import "ChatViewController.h"
@interface HTChatListViewController ()
{
    ChatListCell *cell;
}
@end

@implementation HTChatListViewController

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

static int currentPageNumber = 0;

@synthesize tableView_chatList;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Loading Users
    tableView_chatList.delegate = self;
    arr_userNames = [[NSMutableArray alloc] init];
    arr_userMailIds = [[NSMutableArray alloc] init];
    arr_userIds = [[NSMutableArray alloc] init];
    
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x0A524E);
     searchController = [[UISearchController alloc] initWithSearchResultsController:self];
    searchController.searchResultsUpdater = self;
    searchController.searchBar.placeholder = @"Ask Paper Crunch";
    searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchController.searchBar.barTintColor = [UIColor whiteColor];
    searchController.searchBar.userInteractionEnabled = NO;
    self.navigationItem.titleView = searchController.searchBar;
  
    
    //[self.view.layer setsha]
    currentPageNumber =0;
    
    [self fetchUserData:currentPageNumber];
}

# pragma mark TableView datasource methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger num_sections = 1;
    return num_sections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger num_rows = 0;
    if ([arr_userNames count] > 0) {
        num_rows = [arr_userNames count];
        return num_rows;
    }
    return num_rows;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
        cell.selectedBackgroundView.backgroundColor = [UIColor grayColor];
        cell.imgView_profileImage.image =[UIImage imageNamed:@"ProfileImage.png"];
        cell.lbl_profileName.text =[arr_userNames objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellAccessoryNone;
        
    return cell;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if(self.tableView_chatList.contentOffset.y >= (self.tableView_chatList.contentSize.height - self.tableView_chatList.bounds.size.height)) {
        
        [self fetchUserData:currentPageNumber];
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
// moving to ChatView
    UIStoryboard *storyBoard_main =[UIStoryboard storyboardWithName:@"ChatMain" bundle:[NSBundle mainBundle]];
    HTChatViewController *obj_chatVC = [storyBoard_main instantiateViewControllerWithIdentifier:@"HTChatViewController"];
    
    //ChatViewController
    index_path = indexPath;
    
    //[[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"USERID"];
        [[NSUserDefaults standardUserDefaults] setValue:[arr_userIds objectAtIndex:indexPath.row] forKey:@"SELECTEDUSERID"];
        
        [[NSUserDefaults standardUserDefaults] setValue:[arr_userNames objectAtIndex:indexPath.row] forKey:@"USERNAME"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController pushViewController:obj_chatVC animated:YES];
        
//    }

}

- (void)fetchUserData:(int)pageNumber {
    
    NSString *urlString = [NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_GetLIstOfUsers/user_id/%@/pageno/%li",[[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"],(long)pageNumber];
    NSLog(@"URLSTRING Value is %@",urlString);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSURL *urlObj=[[NSURL alloc]initWithString:urlString];
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    [dataServer setLength:0];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    NSMutableDictionary *response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    // user data

    if ([[response valueForKey:@"message"] isEqualToString:@"Success"]) {
        idarray=[response valueForKey:@"data"];
        for (int i=0; i<=idarray.count-1; i++) {
            // add user details
            [arr_userNames addObject:[[[[idarray objectAtIndex:i] valueForKey:@"firstName"] stringByAppendingString:@" "] stringByAppendingString:[[idarray objectAtIndex:i] valueForKey:@"lastName"]]];
            [arr_userMailIds addObject:[[idarray objectAtIndex:i] valueForKey:@"emailId"]];
            [arr_userIds addObject:[[idarray objectAtIndex:i] valueForKey:@"user_id"]];
        }
        
        NSLog(@"Array Users Count is %lul",(unsigned long)arr_userNames.count);
        
        [tableView_chatList reloadData];
        currentPageNumber++;
    } else {
        
    }
}

- (void)setLoginUserId:(NSString *)str_userId {
    [[NSUserDefaults standardUserDefaults] setValue:str_userId forKey:@"USERID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)setLoginUserId {
    NSString *str_userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return str_userId;
}

@end
