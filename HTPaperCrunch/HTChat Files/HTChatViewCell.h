//
//  ChatViewCell.h
//  ChatBubbles
//
//  Created by HelixTech-Admin on 06/12/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTChatViewCell : UICollectionViewCell

@property UITextView *messageTextView;
@property UIView *bubbleView;
@property NSLayoutConstraint *bubbleViewWidthAnchor;
@property NSLayoutConstraint *bubbleViewRightAnchor;
@property NSLayoutConstraint *bubbleViewLeftAnchor;

@end
