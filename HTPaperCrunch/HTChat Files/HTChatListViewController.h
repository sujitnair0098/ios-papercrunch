//
//  HTChatListViewController.h
//  HTChat
//
//  Created by HelixTech-Admin  on 01/12/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface HTChatListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate, NSURLConnectionDataDelegate, UISearchBarDelegate, UISearchControllerDelegate,UISearchResultsUpdating, UIScrollViewDelegate>
{
    NSMutableData *dataServer;
    NSMutableArray *idarray;
    NSMutableArray *arr_userNames;
    NSMutableArray *arr_userMailIds;
    NSMutableArray *arr_userIds;
    NSIndexPath *index_path;
    UISearchController *searchController;
    BOOL isSearching;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView_chatList;

-(void)setLoginUserId:(NSString *)str_userId;
-(NSString *)getLoginUserId;

- (void)fetchUserData:(int)pageNumber;
@end
