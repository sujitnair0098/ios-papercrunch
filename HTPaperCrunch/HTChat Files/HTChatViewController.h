//
//  HTChatViewController.h
//  HTChat
//
//  Created by HelixTech-Admin  on 01/12/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "HTChatViewCell.h"

@interface HTChatViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, NSURLConnectionDelegate, NSURLConnectionDataDelegate, UITextViewDelegate >
{
    NSMutableDictionary *response;
    NSMutableData *dataServer;
    NSMutableArray *idarray;

    UIAlertController *alertController;
    NSString *str_userId;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomSpacingContraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sendButtonBottomSpacingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomSpacing;

@property (strong, nonatomic) IBOutlet UITableView *tableView_Chat;
@property (strong, nonatomic) IBOutlet UITextView *txt_message;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView_chat;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;



-(void)fetchUserChatData;
- (IBAction)onClickSendMessage:(id)sender;

@end
