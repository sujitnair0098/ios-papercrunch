//
//  ChatListCell.h
//  HTChat
//
//  Created by HelixTech-Admin  on 01/12/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *imgView_profileImage;

@property (strong, nonatomic) IBOutlet UILabel *lbl_profileName;
@end
