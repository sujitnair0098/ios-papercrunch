//
//  WebServiceHandler.m
//  HTWebServiceManager
//
//  Created by HelixTech-Admin on 30/11/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "HTWebServiceHandler.h"

@implementation HTWebServiceHandler

static HTWebServiceHandler *sharedInstance = nil;

+ (HTWebServiceHandler *)sharedInstance {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
            sharedInstance = [[HTWebServiceHandler alloc] initPrivate];
    });
    return sharedInstance;
}

- (instancetype)initPrivate {
    if (self = [super init]) {
        
    }
    return self;
}


- (void)requestWithGetAndURLString:(NSString*)passUrlString andParameters:(NSDictionary *)parametersDictionary andWantReplaceForwarSlash:(BOOL)replaceSlash withResponseType:(HTWebserviceResponseType)responseType withSuccessBlock:(WebServiceHandlerSuccessBlock)success failure:(WebServiceHandlerFailureBlock)failure {
    
    NSString *modifiedURL;
    if (parametersDictionary.count != 0) {
        modifiedURL = [self sendingBackModifiedURLStringWithParametersDictionary:parametersDictionary andURLString:passUrlString];
    } else {
        modifiedURL = [NSString stringWithString:passUrlString];
    }
    
    if (replaceSlash) {
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"&?="];
        modifiedURL = [[modifiedURL componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@"/"];
    }
    
    NSURL *apiURL = [NSURL URLWithString:modifiedURL];
    NSLog(@"APIURL is %@",apiURL);
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL];
    [request setHTTPMethod:@"GET"];
    
    if (responseType == eHTWebserviceResponseTypeJSON) {
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    } else {
        [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];
    }
    
    NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                         {
                                         if (data != nil) {
                                             id tempDict = nil;
                                             tempDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                             if (success != NULL) {
                                                 success(tempDict);
                                             }
                                         } else {
                                             if (failure != NULL) {
                                                 failure(error);
                                             }
                                         }
                                         }];
    [getDataTask resume];

}


- (void)requestWithPostAndURLString:(NSString *)passUrlString andParameters:(NSDictionary *)parametersDictionary andWantReplaceForwarSlash:(BOOL)replaceSlash wantToAttachFilesToBody:(BOOL)isFilesNeedToAttach withNameToFile:(NSString *)nameOfFile withResponseType:(HTWebserviceResponseType)responseType andDataToPost:(NSData *)dataToPost withSuccessBlock:(WebServiceHandlerSuccessBlock)success failure:(WebServiceHandlerFailureBlock)failure{
    
    NSString *modifiedURL;
    if (parametersDictionary.count != 0) {
        modifiedURL = [self sendingBackModifiedURLStringWithParametersDictionary:parametersDictionary andURLString:passUrlString];
    } else {
        modifiedURL = [NSString stringWithString:passUrlString];
    }
    
    if (replaceSlash) {
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"&?="];
        modifiedURL = [[modifiedURL componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@"/"];
    }
    
    NSURL *apiURL = [NSURL URLWithString:modifiedURL];
    NSLog(@"APIURL is %@",apiURL);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:apiURL];
    [urlRequest setHTTPMethod:@"POST"];
    
    if (isFilesNeedToAttach) {
        NSString *boundary = @"YOUR_BOUNDARY_STRING";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [urlRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picture\"; filename=\"%@.png\"\r\n", nameOfFile] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:dataToPost]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [urlRequest addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        [urlRequest setHTTPBody:body];
        
    } else {
        [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }
    
    if (responseType == eHTWebserviceResponseTypeXML) {
            [urlRequest setValue:@"application/xml" forHTTPHeaderField:@"Accept"];
    } else {
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    }
    

    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL];
    NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                         {
                                             if (data != nil) {
                                                 id tempDict = nil;
                                                 tempDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                 if (success != NULL) {
                                                     success(tempDict);
                                                 }
                                             } else {
                                                 if (failure != NULL) {
                                                     failure(error);
                                                 }
                                             }
                                         }];
    [getDataTask resume];
}


- (NSString *)sendingBackModifiedURLStringWithParametersDictionary:(NSDictionary *)parametersDictionary andURLString:(NSString *)passUrlString {
    
    NSMutableArray *urlComponentsArray;
    NSURLComponents *urlComponents;
    NSString *modifyingURL;
    urlComponentsArray = [NSMutableArray array];
    
    for (NSString *key in parametersDictionary) {
        [urlComponentsArray addObject:[NSURLQueryItem queryItemWithName:key value:parametersDictionary[key]]];
    }
    
    urlComponents = [NSURLComponents componentsWithString:passUrlString];
    urlComponents.queryItems = urlComponentsArray;
    modifyingURL = [NSString stringWithFormat:@"%@",urlComponents.URL];
    NSLog(@"Modified URl is %@",modifyingURL);
    return modifyingURL;
}




//Dummy Method

- (void)dummyMethodToTestBlocks:(NSString *)data withSuccessBlock:(WebServiceHandlerSuccessBlock)success {
    
    success(data);
    
}


@end
