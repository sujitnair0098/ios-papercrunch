//
//  SettingsViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 26/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITableViewDataSource>
{
    NSArray * settingsArray;
    NSArray * settingsImageArray;
    
}
@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;

@end
