//
//  BranchViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 25/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "BranchViewController.h"

@interface BranchViewController ()

@end

@implementation BranchViewController
@synthesize arrBranch;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    arrBranch=[[NSMutableArray alloc]initWithObjects:@"Electronics Engineering",@"Chemical Engineering",@"Civil Engineering",@"Computer Engineering",@"Mechanical Enginering",@"Genetic Engineering" ,nil];
    
  
    
   
    
    [_segBranches setSelectedSegmentIndex:0];
    
    [_segBranches setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    
    // color disabled text ---> blue
    [_segBranches setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:37.0/255.0 green:90.0/255.0 blue:84.0/255.0 alpha:1.0] } forState:UIControlStateNormal];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.backgroundColor = [[UIColor colorWithRed:37.0/255.0 green:90.0/255.0 blue:84.0/255.0 alpha:1.0] CGColor];
    bottomBorder.frame = CGRectMake(0, 65, CGRectGetWidth(self.vwBr.frame), 1.0);
    [self.vwBr.layer addSublayer:bottomBorder];

    
      
    UILabel* customTitleView = [[UILabel alloc] initWithFrame:CGRectZero];
    customTitleView.text = @"Branch";
    customTitleView.font = [UIFont systemFontOfSize:20.0 weight:0.1];
    customTitleView.backgroundColor = [UIColor clearColor];
    customTitleView.textColor = [UIColor colorWithRed:194.0/255.0 green:239.0/255.0 blue:231.0/255.0 alpha:1.0];
  
    
    [customTitleView sizeToFit];
    
    self.navigationItem.titleView = customTitleView;
    
    
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                                                         initWithTitle:@"Refine" style:UIBarButtonItemStylePlain target:nil action:nil];
   
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrBranch.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellid=@"cellid";
    
    UITableViewCell *cell=[_tblBranches dequeueReusableCellWithIdentifier:cellid];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellid"];
    }
    
    cell.textLabel.text=[arrBranch objectAtIndex:indexPath.row];
    
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *thisCell = [_tblBranches cellForRowAtIndexPath:indexPath];
    
    
    if (thisCell.accessoryView==nil) {
        thisCell.accessoryView=[[ UIImageView alloc ]
                                   initWithImage:[UIImage imageNamed:@"accessorytickmark"]];
    }
    else{
        thisCell.accessoryView=nil;
    }
}





//-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [_tblBranches cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
//}

@end
