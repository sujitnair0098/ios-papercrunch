//
//  AppDelegate.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 16/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSDKCoreKit.h"
#import "FBSDKLoginKit.h"
#import "HomeViewController.h"
#import "User.h"
#import "SA_MANAGER.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

