//
//  AppDelegate.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 16/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "AppDelegate.h"
#import "HTChatViewController.h"
#import "HTChatListViewController.h"
#import "FeedCapsMenuViewController.h"
#import "PaperViewController.h"
#import "NotificationsViewController.h"
#import "ProfileViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                            didFinishLaunchingWithOptions:launchOptions];
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        // User is logged in, do work such as go to next view controller.
        NSLog(@"User already loged in");
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email,picture"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 
                 NSURL *imageURL=[[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"];
                 NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                 [dict setValue:[result valueForKey:@"name"] forKey:@"username"];
                 [dict setValue:[result valueForKey:@"email"] forKey:@"useremail"];
                 [dict setValue:[result valueForKey:@"id"] forKey:@"userfacebookid"];
                 [dict setValue:imageURL forKey:@"imageurl"];
                 
                 User *currentuser=[[User alloc]initWithDict:dict];
                 [[SA_MANAGER sharedManager].arrUsers addObject:currentuser];
                 
                 
        
        HomeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                 [self.window setRootViewController:vc];

                 // set viewcontrolers for tabbar
               /*  UIStoryboard *storyBoard_chatSDK =[UIStoryboard storyboardWithName:@"ChatMain" bundle:[NSBundle mainBundle]];
                 HTChatListViewController *obj_chatView =[storyBoard_chatSDK instantiateViewControllerWithIdentifier:@"HTChatListViewController"];
                 if ([[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"]) {
                 
                 [obj_chatView setLoginUserId:[[NSUserDefaults standardUserDefaults] valueForKey:@"USERID"]];
                 }
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
                 [obj_chatListView setLoginUserId:[result objectForKey:@"userid"]];
                 
                 UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                 FeedCapsMenuViewController *obj_feedView =[storyBoard instantiateViewControllerWithIdentifier:@"FeedCapsMenuViewController"];
                 PaperViewController *obj_paperView = [storyBoard instantiateViewControllerWithIdentifier:@"PaperViewController"];
                 NotificationsViewController *obj_notificationsView = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationsView"];
                 ProfileViewController *obj_profileView = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                 
                 HomeViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                 
                 vc.viewControllers = @[obj_feedView,obj_paperView,obj_chatView,obj_notificationsView,obj_profileView
                                        ];
                 [[vc.tabBar.items objectAtIndex:2] setTitle:@"Chat"];
                 [[vc.tabBar.items objectAtIndex:2] setImage:[UIImage imageNamed:@"tab75-75chat"]];
                 
                [self.window setRootViewController:vc];*/
        
        
        
             }
             
         }];
         }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    FBSDKLoginManager * loginManager = [[FBSDKLoginManager alloc]init];
    [loginManager logOut];

}




- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings // NS_AVAILABLE_IOS(8_0);
{
    [application registerForRemoteNotifications];
}


-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{

//    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"content---%@", token);

    NSLog(@"deviceToken: %@", deviceToken);
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSLog(@"token:%@",token);
    
    [SA_MANAGER sharedManager].deviceToken = token;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MESSAGERECEIVED" object:nil];

    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    UIApplicationState state = [application applicationState];
    
    // If your app is running
    
    
    if ([[[userInfo objectForKey:@"aps"] allKeys] containsObject:@"type"] && [[[userInfo objectForKey:@"aps"] objectForKey:@"type"] isEqualToString:@"received_message"]) {
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ISCHATVIEW"] == NO) {
        
           
            
        NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
            
        UIAlertController *alertController =[UIAlertController alertControllerWithTitle:@"PaperCrunch" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction =[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [self.window setRootViewController:alertController];
        [self.window makeKeyAndVisible];
     
       
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MESSAGERECEIVED" object:[[userInfo objectForKey:@"aps"] objectForKey:@"message_id"]];
    } else if (state == UIApplicationStateActive) {
        //You need to customize your alert by yourself for this situation. For ex,
        NSString *cancelTitle = @"Close";
        NSString *showTitle = @"Get Photos";
        NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:cancelTitle
                                                  otherButtonTitles:showTitle, nil];
        [alertView show];
       // [alertView release];
        
    }
    // If your app was in in active state
    else if (state == UIApplicationStateInactive)
    {
        
    }
    
    
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    #if DEBUG
        NSLog(@"Register for remote notification failed with error: %@",error.description);
    #endif

}

@end
