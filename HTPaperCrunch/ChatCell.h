//
//  ChatCell.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *userImage;
@property(nonatomic,weak) IBOutlet UILabel *lblUsername;
@property(nonatomic,weak) IBOutlet UILabel *lblMessage;
@property(nonatomic,weak) IBOutlet UILabel *lblTime;

@end
