//
//  SignUpViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertController+Blocks.h"
#import "HomeViewController.h"
#import "MBProgressHUD.h"
#import "SA_MANAGER.h"
#import "User.h"




@interface SignUpViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate,NSURLConnectionDataDelegate, UIScrollViewDelegate>
{
    UIAlertController * alertController;
    UITextField *activeTextField;

}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIView *view_signUp;

@property (strong, nonatomic) IBOutlet UITextField *textFieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *textFieldConfirmPassword;
@property (nonatomic,strong) IBOutlet UITextField *textFieldEmail;


@property (strong, nonatomic) IBOutlet UIButton *buttonImage;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewPlaceholder;
@property(nonatomic,strong) NSMutableDictionary *dataDict;

@property (strong, nonatomic) IBOutlet UIButton *buttonSignUp;

- (IBAction)didTapImageButton:(id)sender;
- (IBAction)didTapSignUpButton:(id)sender;

@end
