//
//  SecondViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "SecondViewController.h"
#import "HTChatListViewController.h"
#import "FeedCapsMenuViewController.h"
#import "PaperViewController.h"
#import "NotificationsViewController.h"
#import "ProfileViewController.h"


@interface SecondViewController ()

@end

@implementation SecondViewController
@synthesize strnm;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    
//    loginButton = [[FBSDKLoginButton alloc] init];
//    loginButton.hidden = YES;
   
          
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapButton1:(id)sender {
    
    
    
    UIButton *resulteButton= (UIButton*)sender;
    NSString *buttonTitle=resulteButton.titleLabel.text;

   
        LogInViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInViewController"];
        [self presentViewController:vc animated:YES completion:nil];

}

- (IBAction)didTapButton2:(id)sender {
    
    
  
        FBSDKLoginManager * loginManager = [[FBSDKLoginManager alloc]init];
        [loginManager logInWithReadPermissions:@[@"email",@"public_profile",@"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error)
            {
                NSLog(@"Process Error :%@",error.localizedDescription);
                UIAlertController *alertController =[UIAlertController alertControllerWithTitle:@"FB Login Error" message:error.description preferredStyle:UIAlertControllerStyleAlert];
                                                      UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIPreviewActionStyleDefault handler:nil];
                [alertController addAction:alertAction];
                [self presentViewController:alertController animated:YES completion:nil];
                                                      
            }
            else if (result.isCancelled)
            {
                NSLog(@"Cancelled");
            }
            else
            {
                //[self getFacebookProfileInfo];
                
                if ([result.grantedPermissions containsObject:@"email"]) {
                    if ([FBSDKAccessToken currentAccessToken]) {
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email,picture"}]
                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                             if (!error) {
                                 NSLog(@"fetched user:%@", result);
                                 
                                 NSString* nameStr = [result valueForKey:@"name"];
                                 NSArray* firstLastStrings = [nameStr componentsSeparatedByString:@" "];
                                 NSString* firstName = [firstLastStrings objectAtIndex:0];
                                 NSString* lastName = [firstLastStrings objectAtIndex:1];
                                 NSString *userEmailId= [result valueForKey:@"email"];
                                 NSString *fbId =[result valueForKey:@"id"];
                                 
                                 NSString *strToken=[SA_MANAGER sharedManager].deviceToken;
                                 
                                 NSURL *imageURL=[[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"];
                                 
                                 // NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                                 // UIImage *userImage = [UIImage imageWithData:imageData];
                                 
                                 NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                                 [dict setValue:[result valueForKey:@"name"] forKey:@"username"];
                                 [dict setValue:[result valueForKey:@"email"] forKey:@"useremail"];
                                 [dict setValue:[result valueForKey:@"id"] forKey:@"userfacebookid"];
                                 [dict setValue:imageURL forKey:@"imageurl"];
                                 // [dict setValue:firstName forKey:@"firstName"];
                                 //[dict setValue:lastName forKey:@"lastName"];
                                 
//                                 User *currentuser=[[User alloc]initWithDict:dict];
//                                 [[SA_MANAGER sharedManager].arrUsers addObject:currentuser];
                                 
                                 // Send user data to server
                                 
                                 NSString *strUrl=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_UserSignupWithFB?firstName=%@&lastName=%@&emailId=%@&ProfilePicLink=%@&fbID=%@&deviceToken=%@&deviceType=1",firstName,lastName,userEmailId,imageURL,fbId,strToken];
                                 
                                 NSLog(@"user data:%@", strUrl);
                                 
                                 NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
                                 
                                 NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:strUrl,@"rqBody", nil];
                                 
                                 
                                 
                                 
                                 NSError *error=nil;
                                 
                                 // NSData *postData=[NSJSONSerialization dataWithJSONObject:requestData options:kNilOptions error:&error];
                                 NSData *postData=[strUrl dataUsingEncoding:NSUTF8StringEncoding];
                                 
                                 
                                 NSString *jsonString = [[NSString alloc] initWithData:postData
                                                                              encoding:NSUTF8StringEncoding];
                                 NSLog(@"Response JSON=%@", jsonString);
                                 
                                 
                                 
                                 NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
                                 
                                 [request setHTTPMethod:@"POST"];
                                 [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                                 [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                                 [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
                                 [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
                                 [request setHTTPBody:postData];
                                 
                                 NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                 
                                 
                                 NSURLResponse *response;
                                 
                                 NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                                 
                                 NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
                                 
                                 NSDictionary *result_response = [NSJSONSerialization JSONObjectWithData:[resSrt dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
                                 NSLog(@"%@",result_response);
                                 NSString *codeR = [[result_response objectForKey:@"response"] stringValue];
                                 
                                 //set user
                                 [dict setValue:[result_response objectForKey:@"userId"] forKey:@"userId"];
                                 User *currentuser=[[User alloc]initWithDict:dict];
                                 [[SA_MANAGER sharedManager].arrUsers addObject:currentuser];
                                 
                                 // mysterious missing code
                                 if ([codeR isEqualToString:@"1"])
                                     
                                 {
                                     HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
                                     [obj_chatListView setLoginUserId:[result_response objectForKey:@"userid"]];
                                     
                                     
                                     HomeViewController*vc=[[HomeViewController alloc]init];
                                     vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                     [self presentViewController:vc animated:YES completion:nil];
                                     
                                     
                                     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                     
                                     
                                    /* UIStoryboard *storyBoard_chatSDK =[UIStoryboard storyboardWithName:@"ChatMain" bundle:[NSBundle mainBundle]];
                                     HTChatListViewController *obj_chatView =[storyBoard_chatSDK instantiateViewControllerWithIdentifier:@"HTChatListViewController"];
                                     HTChatListViewController *obj_chatListView =[[HTChatListViewController alloc] init];
                                     [obj_chatListView setLoginUserId:[result_response objectForKey:@"userid"]];
                                     
                                     UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                                     FeedCapsMenuViewController *obj_feedView =[storyBoard instantiateViewControllerWithIdentifier:@"FeedCapsMenuViewController"];
                                     PaperViewController *obj_paperView = [storyBoard instantiateViewControllerWithIdentifier:@"PaperViewController"];
                                     NotificationsViewController *obj_notificationsView = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationsView"];
                                     ProfileViewController *obj_profileView = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                                     
                                     HomeViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                     vc.viewControllers = @[obj_feedView,obj_paperView,obj_chatView,obj_notificationsView,obj_profileView
                                                            ];
                                     //        [[vc.tabBar.items objectAtIndex:0] setBadgeValue:@"Feed"];
                                     //        [[vc.tabBar.items objectAtIndex:1] setBadgeValue:@"Papers"];
                                     [[vc.tabBar.items objectAtIndex:2] setTitle:@"Chat"];
                                     [[vc.tabBar.items objectAtIndex:2] setImage:[UIImage imageNamed:@"tab75-75chat"]];
                                     
                                     //        [[vc.tabBar.items objectAtIndex:3] setBadgeValue:@"Notifications"];
                                     //        [[vc.tabBar.items objectAtIndex:4] setBadgeValue:@"Profile"];
                                     //
                                     
                                     
                                     [self presentViewController:vc animated:YES completion:nil];*/

                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                 }
                                 
                             }
                         }];
                    }
               
                
                }
     

    }
        }];
}

-(void)viewWillAppear:(BOOL)animated {

    [MBProgressHUD hideHUDForView:self.view animated:YES];

}

@end
