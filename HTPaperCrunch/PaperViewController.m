//
//  PaperViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 13/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "PaperViewController.h"

@interface PaperViewController ()

@end

@implementation PaperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrbuttons=[[NSMutableArray alloc]initWithObjects:@"Paper",@"Bookmarks",@"Saved",@"Backlog", nil];
    
    UIScrollView  *scrollVie=[[UIScrollView alloc]init];
    
    scrollVie.delegate = self;
    scrollVie.scrollEnabled = YES;
    int scrollWidth = 100;
    //scrollVie.contentSize = CGSizeMake(scrollWidth,100);
    scrollVie.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 28);
    scrollVie.backgroundColor=[UIColor colorWithRed:7.0/255.0 green:84.0/255.0 blue:78.0/255.0 alpha:1.0];

    for(int index=0; index < 4; index++)
    {
        CGFloat xOffset = index*self.view.frame.size.width/3 ;
       
        btn = [self createbuttonWithTag:index];
        
        [btn addTarget:self
                action:@selector(buttonclicked:)
      forControlEvents:UIControlEventTouchDown];
        btn.backgroundColor=[UIColor colorWithRed:7.0/255.0 green:84.0/255.0 blue:78.0/255.0 alpha:1.0];
        btn.frame=CGRectMake(xOffset, 0, [UIScreen mainScreen].bounds.size.width/3, 28);
        [btn setTitle:[arrbuttons objectAtIndex:index] forState:normal];
        
        [btn setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
        //[btn.titleLabel setFont:[UIFont systemFontOfSize:17 weight:0.1]];
        
        [btn.titleLabel setFont:[UIFont fontWithName:@"MyriadPro" size:17]];
       
        
        [scrollVie addSubview:btn];
        
    }
    
    [self.view addSubview:scrollVie];
    
    scrollVie.contentSize = CGSizeMake(btn.frame.size.width*4,28);
    // Do any additional setup after loading the view, typically from a nib.
    
    
    [btnPaper setTitleColor:[UIColor whiteColor] forState:normal];
//    [btnPaper setImage:[UIImage imageNamed:@"paper.png"] forState:UIControlStateNormal];
//    [btnBookmarks setImage:[UIImage imageNamed:@"bookmkgrn.png"] forState:UIControlStateNormal];
//    
//    [btnSave setImage:[UIImage imageNamed:@"sveoffgrn.png"] forState:UIControlStateNormal];
//    
//    [btnBacklog setImage:[UIImage imageNamed:@"backloggrn.png"] forState:UIControlStateNormal];
    
    self.mypaper.hidden=NO;
    self.bookmarks.hidden=YES;
    self.saveoffline.hidden=YES;
    self.backlog.hidden=YES;
    // Do any additional setup after loading the view.
    
    _scrlMenu.delegate=self;
    
            
     isSelected=NO;
    
//    UIBarButtonItem *btnRight=[[UIBarButtonItem alloc]init];
//    self.navigationItem.rightBarButtonItem=btnRight;
//    
    searchController = [[UISearchController alloc] initWithSearchResultsController:self];
 
    searchController.searchResultsUpdater = self;
    searchController.searchBar.placeholder = @"Search Papers";
    searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchController.searchBar.barTintColor = [UIColor whiteColor];
        self.definesPresentationContext = YES;
    searchController.searchBar.delegate=self;
    searchController.delegate=self;
    
   // UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:searchController.searchBar];
   
    self.navigationItem.titleView = searchController.searchBar;
  
    
    UIBarButtonItem *btnRefine=[[UIBarButtonItem alloc]init];
    [btnRefine setWidth:50];
    
    
    [btnRefine setImage:[UIImage imageNamed:@"35-30filtergreen.png"]];
   //[btnRefine setTarget:self action:@selector(btnRefineClicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem=btnRefine;
    
    [btnRefine setTarget:self];
    [btnRefine setAction:@selector(btnRefineClicked)];
    
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                                   initWithTarget:self
//                                   action:@selector(dismissKeyboard)];
//    
//    [self.view addGestureRecognizer:tap];
    
    MyPapersViewController *mpvc=[[MyPapersViewController alloc]init];
    
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{

    [searchController dismissViewControllerAnimated:YES completion:nil];

}

-(void)btnRefineClicked{

    //[self performSegueWithIdentifier:@"refine" sender:self];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RefineViewController *myNewVC = (RefineViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Refine"];
    [self.navigationController pushViewController:myNewVC animated:YES];
   
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"refine"]) {
        RefineViewController *vc = [segue destinationViewController];
    }

}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //To disable vertical scrolling
    [_scrlMenu setContentOffset: CGPointMake(_scrlMenu.contentOffset.x, oldy)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)btnPaperClicked:(id)sender{

   
    self.mypaper.hidden=NO;
    self.bookmarks.hidden=YES;
    self.saveoffline.hidden=YES;
    self.backlog.hidden=YES;
    
 
        [btnPaper setTitleColor:[UIColor whiteColor] forState:normal];
        //[btnPaper setImage:[UIImage imageNamed:@"paper.png"] forState:UIControlStateSelected];
   

        [btnBookmarks setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnSave setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnBacklog setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    
//    [btnPaper setImage:[UIImage imageNamed:@"paper.png"] forState:UIControlStateNormal];
//    [btnBookmarks setImage:[UIImage imageNamed:@"bookmkgrn.png"] forState:UIControlStateNormal];
//
//    [btnSave setImage:[UIImage imageNamed:@"sveoffgrn.png"] forState:UIControlStateNormal];
//
//    [btnBacklog setImage:[UIImage imageNamed:@"backloggrn.png"] forState:UIControlStateNormal];


    }


-(IBAction)btnBookmarksClicked:(id)sender{

    self.mypaper.hidden=YES;
    self.bookmarks.hidden=NO;
    self.saveoffline.hidden=YES;
    self.backlog.hidden=YES;
    [btnBookmarks setTitleColor:[UIColor whiteColor] forState:normal];
    //[btnBookmarks setImage:[UIImage imageNamed:@"bookmk.png"] forState:UIControlStateSelected];
    [btnPaper setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnSave setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnBacklog setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    
//    [btnPaper setImage:[UIImage imageNamed:@"papergrn.png"] forState:UIControlStateNormal];
//    [btnBookmarks setImage:[UIImage imageNamed:@"bookmk.png"] forState:UIControlStateNormal];
//    
//    [btnSave setImage:[UIImage imageNamed:@"sveoffgrn.png"] forState:UIControlStateNormal];
//    
//    [btnBacklog setImage:[UIImage imageNamed:@"backloggrn.png"] forState:UIControlStateNormal];
    
}
-(IBAction)btnSaveClicked:(id)sender{

    self.mypaper.hidden=YES;
    self.bookmarks.hidden=YES;
    self.saveoffline.hidden=NO;
    self.backlog.hidden=YES;
    [btnSave setTitleColor:[UIColor whiteColor] forState:normal];
    //[btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
    [btnBookmarks setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnPaper setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnBacklog setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];

    
//    [btnPaper setImage:[UIImage imageNamed:@"papergrn.png"] forState:UIControlStateNormal];
//    [btnBookmarks setImage:[UIImage imageNamed:@"bookmkgrn.png"] forState:UIControlStateNormal];
//    
//    [btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateNormal];
//    
//    [btnBacklog setImage:[UIImage imageNamed:@"backloggrn.png"] forState:UIControlStateNormal];
//    
}
-(IBAction)btnBAcklogClicked:(id)sender{
    self.mypaper.hidden=YES;
    self.bookmarks.hidden=YES;
    self.saveoffline.hidden=YES;
    self.backlog.hidden=NO;
[btnBacklog setTitleColor:[UIColor whiteColor] forState:normal];
   // [btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
    [btnBookmarks setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnSave setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [btnPaper setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    
//    [btnPaper setImage:[UIImage imageNamed:@"papergrn.png"] forState:UIControlStateNormal];
//    [btnBookmarks setImage:[UIImage imageNamed:@"bookmkgrn.png"] forState:UIControlStateNormal];
//    
//    [btnSave setImage:[UIImage imageNamed:@"sveoffgrn.png"] forState:UIControlStateNormal];
//    
//    [btnBacklog setImage:[UIImage imageNamed:@"backlog.png"] forState:UIControlStateNormal];
}

-(IBAction)buttonclicked:(id)sender {
    
    UIButton *btntag=(UIButton *)sender;
    
    
    if (btntag.tag==0) {
        self.mypaper.hidden=NO;
        self.bookmarks.hidden=YES;
        self.saveoffline.hidden=YES;
        self.backlog.hidden=YES;
        
    
//        [btntag setTitleColor:[UIColor whiteColor] forState:normal];
//        //[btnPaper setImage:[UIImage imageNamed:@"paper.png"] forState:UIControlStateSelected];
//    
//       
//
//        
//        [ setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        
    }
//        [[btn viewWithTag:2] setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [[btn viewWithTag:3] setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];

    
    if ([btntag tag] == 1) {
        self.mypaper.hidden=YES;
        self.bookmarks.hidden=NO;
        self.saveoffline.hidden=YES;
        self.backlog.hidden=YES;
       // [btntag setTitleColor:[UIColor whiteColor] forState:normal];
        //[btnBookmarks setImage:[UIImage imageNamed:@"bookmk.png"] forState:UIControlStateSelected];
       
    
    
       
        //[ setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [[btn viewWithTag:2] setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [[btn viewWithTag:3] setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    
            }
    if ([btntag tag] == 2) {
        self.mypaper.hidden=YES;
        self.bookmarks.hidden=YES;
        self.saveoffline.hidden=NO;
        self.backlog.hidden=YES;
//        [btntag setTitleColor:[UIColor whiteColor] forState:normal];
//        //[btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
//        [btnBookmarks setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [btnPaper setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [btnBacklog setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
        
    }
    if ([btntag tag] == 3) {
        self.mypaper.hidden=YES;
        self.bookmarks.hidden=YES;
        self.saveoffline.hidden=YES;
        self.backlog.hidden=NO;
//        [btntag setTitleColor:[UIColor whiteColor] forState:normal];
//        // [btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
//        [btnBookmarks setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [btnSave setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//        [btnPaper setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
//    
}
}

-(UIButton *)createbuttonWithTag:(NSInteger)tag{
    
    
    
    UIButton *btnScroll=[[UIButton alloc]init] ;

    btnScroll.tag=tag;
    
    
    return btnScroll;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
