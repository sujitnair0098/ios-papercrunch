//
//  BookmarksViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 22/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "BookmarksViewController.h"

@interface BookmarksViewController ()

@end

@implementation BookmarksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrCell=[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)Passcell:(UITableViewCell *)cell{
//
//    [arrCell addObject:cell];
//
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return arrCell.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   UITableViewCell *newcell=[arrCell objectAtIndex:indexPath.row];
    
    NSString *cellid=@"cellid";
    
    newcell=[tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (newcell==nil) {
        newcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    
    return newcell;

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
