//
//  HomeViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender 
 {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
     UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:self];
     searchController.searchResultsUpdater = self;
     
     searchController.searchBar.placeholder = @"Ask Paper Crunch";
     searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
     searchController.searchBar.barTintColor = [UIColor whiteColor];
     
     self.navigationItem.titleView = searchController.searchBar;
     self.definesPresentationContext = YES;

}

-(void)viewDidAppear:(BOOL)animated{

    [MBProgressHUD hideHUDForView:self.view animated:YES];

}


@end
