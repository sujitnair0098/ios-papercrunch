//
//  MessagesViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 22/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "MessagesViewController.h"

@interface MessagesViewController ()

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrMessages=[[NSMutableArray alloc]init];
    
       self.navigationItem.title= _strUser;
    _txtToSend.delegate=self;
    // Do any additional setup after loading the view.
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self serverChecking];
    [_tblMessages reloadData];
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([response objectForKey:@"response"]==0) {
        return 1;
    }
    return idarray.count;
    }


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  static  NSString *cellid = @"cellid";
  
    UITableViewCell *cell=[_tblMessages dequeueReusableCellWithIdentifier:cellid];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    cell.textLabel.text= [[idarray objectAtIndex:indexPath.row]valueForKey:@"message"];
    
    
    
 
    return cell;



}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_txtToSend resignFirstResponder];
    return YES;
}

-(IBAction)btnSendClicked:(id)sender{
    
    
    if (_txtToSend.text.length !=0){
    
       
        [self sendmessages];
        [_tblMessages reloadData];
    
    }
    
    else{
    
        NSLog(@"Please type something");
    
    }


}

-(void)sendmessages{

    
        
        NSString *myString = [_touser stringValue];
        NSString *fromuser =[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"userId"];
        
        
        
        
        
        NSString *strUrl=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_SendMessage/sender_id/%@/receiver_id/%d/message/%@",fromuser ,300,_txtToSend.text];
        
        NSLog(@"user data:%@", strUrl);
        
        NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:strUrl,@"rqBody", nil];
        
        
        
        
        NSError *error=nil;
        
        // NSData *postData=[NSJSONSerialization dataWithJSONObject:requestData options:kNilOptions error:&error];
        NSData *postData=[strUrl dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSString *jsonString = [[NSString alloc] initWithData:postData
                                                     encoding:NSUTF8StringEncoding];
        NSLog(@"Response JSON=%@", jsonString);
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
       // NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        
        NSURLResponse *response;
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
        
        NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[resSrt dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        NSLog(@"%@",result);
        
   
        NSString *codeR = [[result objectForKey:@"response"] stringValue];
        // mysterious missing code
        if ([codeR isEqualToString:@"1"])
            
        {
            
            NSLog(@"sent");
        
            [_tblMessages reloadData];
            _txtToSend.text=nil;
            
            [self serverChecking];
            
        }
        
        else
        {
         
            
            NSLog(@"failed to connect");
            
            alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Sorry could not establish connection to server" preferredStyle:UIAlertControllerStyleAlert];
            
            //Below code ll applicable for only iPads.{
            alertController.popoverPresentationController.sourceView = self.view;
            alertController.popoverPresentationController.sourceRect = _txtToSend.frame;
            //}
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:ok];
 
            
            
        }
        
    }




-(void)serverChecking{
    
    NSString *logedinuser=[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"userId"];
    NSString *urlString=[NSString stringWithFormat:@"http://httest.in/papercrunchws/RUC_GetUserMessages/loggedin_user_id/%@/other_user_id/%d",logedinuser,300];
  
    NSURL *urlObj=[[NSURL alloc]initWithString:urlString];
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
    [dataServer setLength:0];
    
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    NSString *strresp=[[response objectForKey:@"response"]stringValue];
    
    if ([strresp isEqualToString:@"0"]) {
        
        
        NSLog(@"No records found");
    }
    
    else{
    
    idarray=[response objectForKey:@"data"];
        for(int i = 0; i < idarray.count; i++) {
        if([[(NSString *) [idarray objectAtIndex:i] valueForKey:@"data"] isEqualToString:@"No records found in booking master"]){
            

        NSLog(@"no messages found");
        
        
    }
    
    else{
      
        
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        
        Message *msg= [[Message alloc]initWithDict:dict];
        
        [dict setValue:[[idarray objectAtIndex:i] valueForKey:@"message"] forKey:@"strMessage"];
        [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"message_id"] forKey:@"strMessageid"];
        [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"receiver_id"] forKey:@"strReceiverid"];
        [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"sender_id"] forKey:@"strSenderid"];
        [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"sent_date"] forKey:@"strSentdate"];
        
        
        [[SA_MANAGER sharedManager].arrMessagedata addObject:msg];
        
        [arrMessages addObject:[[idarray objectAtIndex:i] valueForKey:@"message"]];
        
        
    
        
        [_tblMessages reloadData];
    }
    }
    }
    
}












/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
