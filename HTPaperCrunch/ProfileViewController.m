//
//  ProfileViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 13/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize carousel;
@synthesize items,strName;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _scrollView.delegate = self;
    
    
    
    if ([FBSDKAccessToken currentAccessToken ]) {
   
        if ([SA_MANAGER sharedManager].arrUsers !=nil) {

    _lblName.text=[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"username"];
            NSURL *pictureURL = [NSURL URLWithString:[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"imageurl"]];
            NSData *imageData = [NSData dataWithContentsOfURL:pictureURL];
            UIImage *fbImage = [UIImage imageWithData:imageData];
            _imgProfile.image=fbImage;
        
        }
    }
    
    else{
    
        if ([SA_MANAGER sharedManager].arrUsers !=nil) {
            NSString *userfirstName=[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"firstname"];
            NSString *userlastName=[[[SA_MANAGER sharedManager].arrUsers objectAtIndex:0]valueForKey:@"lastname"];
            _lblName.text=[[userfirstName stringByAppendingString:@" "]stringByAppendingString:userlastName];
            
        }
    
    
    
    }
    
    _lblSchool.text=_strSchool;
    _lblCollege.text=_strCollege;
    _lblArea.text=_strArea;
    
    
    
        
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];

    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    bottomBorder.frame = CGRectMake(0, 30, CGRectGetWidth(self.vwAbout.frame), 1.0);
    [self.vwAbout.layer addSublayer:bottomBorder];
    
    CALayer *topborder = [CALayer layer];
    topborder.backgroundColor = [UIColor lightGrayColor].CGColor;
    topborder.frame = CGRectMake(0, 1, CGRectGetWidth(self.vwAbout.frame), 1.0);
    [self.vwAbout.layer addSublayer:topborder];
    

}

-(void)dotmenuclicked{

    [self performSegueWithIdentifier:@"Logout" sender:self];

}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //To disable vertical scrolling
    [scrollView setContentOffset: CGPointMake(scrollView.contentOffset.x, oldY)];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)awakeFromNib
//For carousel
{
    [super awakeFromNib];
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    self.items = [NSMutableArray array];

    for (int i = 0; i < 6; i++)
    {
        [items addObject:@(i)];
    }
}

- (void)dealloc
{
    [searchController.view removeFromSuperview];    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    carousel.delegate = nil;
    carousel.dataSource = nil;
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    //create new view if no view is available for recycling
    if (view == nil)
    {
//        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
//        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
//        view.contentMode = UIViewContentModeCenter;
        
        
        
        
        
//        view = [[UIView alloc] initWithFrame:view.bounds];
//        view.contentMode = UIViewContentModeCenter;
//
//        [view addSubview:_viewFirst];
        
        
        
    }
    else
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeCenter;
        
//        view = [[UIView alloc] initWithFrame:view.bounds];
//        [view addSubview:_viewFirst];
        
        //get a reference to the label in the recycled view
//        [view addSubview:_viewFirst];
//        
//        _viewFirst = [[UIView alloc]initWithFrame:view.bounds];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    return view;
}

-(IBAction)btnProfileClicked:(id)sender{

    [_btnProfile setTitleColor:[UIColor whiteColor] forState:normal];
    // [btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
    [_btnSattistics setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [_btnNotifs setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
   }
-(IBAction)btnStatisticsClicked:(id)sender{

    [_btnSattistics setTitleColor:[UIColor whiteColor] forState:normal];
    // [btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
    [_btnProfile setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [_btnNotifs setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];


}
-(IBAction)btnNotifsClicked:(id)sender{

    [_btnNotifs setTitleColor:[UIColor whiteColor] forState:normal];
    // [btnSave setImage:[UIImage imageNamed:@"sveoff.png"] forState:UIControlStateSelected];
    [_btnSattistics setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
    [_btnProfile setTitleColor:[UIColor colorWithRed:0.76 green:0.94 blue:0.91 alpha:1.0] forState:normal];
 


}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
