//
//  MessagesViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 22/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SA_MANAGER.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "Message.h"
#import "UIAlertController+Blocks.h"




@interface MessagesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,NSURLConnectionDataDelegate>{

    IBOutlet UIButton *btnSend;
    NSMutableArray *arrMessages;
    NSMutableData *dataServer;
    NSMutableArray *idarray;
    NSMutableArray *messageArray;
    UIAlertController * alertController;
    NSMutableDictionary *response;
    UISearchController *srchcontroller;
}

@property(nonatomic,strong) IBOutlet UITableView *tblMessages;
@property(nonatomic,strong) IBOutlet UITextField *txtToSend;
@property(nonatomic,strong) NSString *strUser;
@property(nonatomic,strong) NSNumber *touser;

-(IBAction)btnSendClicked:(id)sender;
@end
