//
//  TutorViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 20/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "TutorViewController.h"

@interface TutorViewController ()

@end

@implementation TutorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_imageViewTutor setImage:[UIImage imageNamed:@"tut1.png"]];
    _label1.text = @"Sign up to create your free acount";
    [_imageViewProgress setImage:[UIImage imageNamed:@"progress1.png"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didSkip:(id)sender {
    
    SecondViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    
    vc.str1 = [NSString stringWithFormat:@"SIGN UP WITH EMAIL"];
    vc.str2 = [NSString stringWithFormat:@"SIGN UP WITH FACEBOOK"];
    
    [self presentViewController:vc animated:YES completion:nil];
    
//    UIWindow * window =  [[[UIApplication sharedApplication] windows] firstObject];
//    window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
//      [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)didNext:(id)sender {
    
    if ([_imageViewTutor.image isEqual:[UIImage imageNamed:@"tut1.png"]]) {
        [_imageViewTutor setImage:[UIImage imageNamed:@"tut2.png"]];
         _label1.text = @"Enter your details and seat back";
        [_imageViewProgress setImage:[UIImage imageNamed:@"progress2"]];
    }
    else if ([_imageViewTutor.image isEqual:[UIImage imageNamed:@"tut2.png"]]){
        [_imageViewTutor setImage:[UIImage imageNamed:@"tut3.png"]];
        _label1.text = @"Papercrunch create your personalised repository tailor made to suitable only you";
        [_imageViewProgress setImage:[UIImage imageNamed:@"progress3"]];

    }
    else if ([_imageViewTutor.image isEqual:[UIImage imageNamed:@"tut3.png"]]){
        [_imageViewTutor setImage:[UIImage imageNamed:@"tut4.png"]];
        _label1.text = @"Learn and Party";
        [_imageViewProgress setImage:[UIImage imageNamed:@"progress4"]];

    }
    else if ([_imageViewTutor.image isEqual:[UIImage imageNamed:@"tut4.png"]]){
        
        SecondViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
        
        vc.str1 = [NSString stringWithFormat:@"SIGN UP WITH EMAIL"];
        vc.str2 = [NSString stringWithFormat:@"SIGN UP WITH FACEBOOK"];

        [self presentViewController:vc animated:YES completion:nil];
        
    }
}
@end
