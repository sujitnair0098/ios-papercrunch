//
//  PaperViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 13/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RefineViewController.h"
#import "MyPapersViewController.h"


@interface PaperViewController : UIViewController<UISearchBarDelegate,UISearchResultsUpdating,UIScrollViewDelegate,UISearchControllerDelegate>
{
    float oldy;
    IBOutlet UIButton *btnPaper;
    IBOutlet UIButton *btnBookmarks;
    IBOutlet UIButton *btnSave;
    IBOutlet UIButton *btnBacklog;
    BOOL isSelected;
    UISearchController *searchController;
    NSMutableArray *arrSearch;
    NSMutableArray *arrbuttons;
    UIButton *btn;
    
}

@property(nonatomic,weak) IBOutlet UIView *mypaper;
@property(nonatomic,weak) IBOutlet UIView *bookmarks;
@property(nonatomic,weak) IBOutlet UIView *saveoffline;
@property(nonatomic,weak) IBOutlet UIView *backlog;

@property(nonatomic,weak) IBOutlet UISegmentedControl *segment;

-(IBAction)btnPaperClicked:(id)sender;
-(IBAction)btnBookmarksClicked:(id)sender;
-(IBAction)btnSaveClicked:(id)sender;
-(IBAction)btnBAcklogClicked:(id)sender;


@property(nonatomic,strong)IBOutlet UIScrollView *scrlMenu;
@property(nonatomic,strong) IBOutlet UIView *contentview;

@end
