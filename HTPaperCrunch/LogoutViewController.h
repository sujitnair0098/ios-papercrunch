//
//  LogoutViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 10/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondViewController.h"
#import "FBSDKLoginKit.h"
#import "FBSDKCoreKit.h"
#import "MBProgressHUD.h"


@interface LogoutViewController : UIViewController
@property(nonatomic,weak) IBOutlet UIView *popview;
@property(nonatomic,weak) IBOutlet UIButton *btnLogout;

-(IBAction)btnLogoutClicked:(id)sender;

@end
