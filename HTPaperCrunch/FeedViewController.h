//
//  FeedViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/09/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"


@interface FeedViewController : UIViewController<UISearchControllerDelegate, UISearchResultsUpdating,UIScrollViewDelegate,UISearchBarDelegate>
{
    float oldY;
    
    UISearchController *searchController;
}

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;



- (IBAction)didChangeValue:(UISegmentedControl *)sender;

@property (strong, nonatomic) IBOutlet UILabel *label;
- (IBAction)didTapSettingButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewFeedSegment;
@property (strong, nonatomic) IBOutlet UIButton *labelFeedSegment;
- (IBAction)didTapFeedSegmentButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewBookmarkSegment;
@property (strong, nonatomic) IBOutlet UIButton *labelBookmarkSegment;
- (IBAction)didTapBookmarkSegmentButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewTrendingSegment;
@property (strong, nonatomic) IBOutlet UIButton *labelTrendingSegment;
@property(nonatomic,strong) UINavigationController* homeNav;

- (IBAction)didTapTrendingSegmentButton:(id)sender;



@end
