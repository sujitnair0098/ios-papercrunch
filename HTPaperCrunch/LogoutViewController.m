//
//  LogoutViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 10/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "LogoutViewController.h"

@interface LogoutViewController ()

@end

@implementation LogoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *transparencyButton = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    transparencyButton.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:transparencyButton belowSubview:_popview];
    [transparencyButton addTarget:self action:@selector(dismissHelper:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnLogoutClicked:(id)sender{


    [FBSDKAccessToken setCurrentAccessToken:nil];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    SecondViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    [self presentViewController:vc animated:YES completion:nil];
    
    NSLog(@"Logged out of facebook");
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:
                                [NSURL URLWithString:@"http://login.facebook.com"]];
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }   


}

- (void)dismissHelper:(UIButton *)sender
{
    // [self.view dismiss];
    
    [self dismissModalViewControllerAnimated:NO];
    sender.hidden = YES;
    // or [sender removeFromSuperview]
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
