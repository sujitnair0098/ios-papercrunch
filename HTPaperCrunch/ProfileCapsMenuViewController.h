//
//  ProfileCapsMenuViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 25/11/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "SatisticsViewController.h"
#import "NotificationsViewController.h"
#import "ProfileViewController.h"

@interface ProfileCapsMenuViewController : UIViewController<UISearchControllerDelegate,UISearchResultsUpdating>
{
UISearchController *searchController;
}
@property(nonatomic,strong)CAPSPageMenu *pagemenu;

@end
