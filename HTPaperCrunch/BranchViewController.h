//
//  BranchViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 25/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BranchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{


    NSMutableArray *arrBranch;


}

 



@property(nonatomic,weak) IBOutlet UITableView *tblBranches;
@property(nonatomic,weak) IBOutlet UISegmentedControl *segBranches;
@property(nonatomic,strong) IBOutlet UIView *vwBr;
@property(nonatomic,retain) NSMutableArray *arrBranch;

@end
