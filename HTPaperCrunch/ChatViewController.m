//
//  ChatViewController.m
//  HTPaperCrunch
//
//  Created by HelixTech-User on 13/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController ()

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   searchController = [[UISearchController alloc] initWithSearchResultsController:self];
    searchController.searchResultsUpdater = self;
    searchController.searchBar.placeholder = @"Ask Paper Crunch";
    searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchController.searchBar.barTintColor = [UIColor whiteColor];
    self.navigationItem.titleView = searchController.searchBar;
    self.definesPresentationContext = YES;
    
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    
    arrImages=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"avatar"], nil];
  

    UINib *cellNib = [UINib nibWithNibName:@"ChatCell" bundle:nil];
    [self.tblChatList registerNib:cellNib forCellReuseIdentifier:@"cellid"];
    
    selectedrow=0;

    [self serverChecking];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return idarray.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

     NSString *cellid=@"cellid";
    ChatCell *cell=[_tblChatList dequeueReusableCellWithIdentifier:@"cellid"];
    
    if (cell==nil) {
        cell=[[ChatCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    
    cell.userImage.image=[arrImages objectAtIndex:0];
    
    
  
    cell.lblUsername.text= [[[[idarray objectAtIndex:indexPath.row]valueForKey:@"firstName"]stringByAppendingString:@" "]stringByAppendingString:[[idarray objectAtIndex:indexPath.row]valueForKey:@"lastName"]];

//    NSDateFormatter *date = [[NSDateFormatter alloc] init];
//    [date setDateFormat:@"HH:mm"];
//    NSString *dateString = [date stringFromDate:[NSDate date]];
//    [cell.lblTime setText:dateString];
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 75;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

   
    
    [self performSegueWithIdentifier:@"chat" sender:self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"chat"]) {
        NSIndexPath *ip=[_tblChatList indexPathForSelectedRow];
        MessagesViewController *mvc= (MessagesViewController *)segue.destinationViewController;
         _stringtoPass=[[idarray objectAtIndex:ip.row ]valueForKey:@"firstName"];
        userNumb=[[idarray objectAtIndex:ip.row]valueForKey:@"user_id"];
        mvc.strUser= _stringtoPass;
        mvc.touser=userNumb;
        
    }



}

-(void)serverChecking{
    
    NSURL *urlObj=[[NSURL alloc]initWithString:@"http://httest.in/papercrunchws/RUC_GetLIstOfUsers/user_id/230/pageno/0"];
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
    [dataServer setLength:0];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableDictionary *response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    
    
    idarray=[response valueForKey:@"data"];
    
  
    for (int i=0; i<=idarray.count-1; i++) {
        
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    Chat *userChat= [[Chat alloc]initWithDict:dict];
        
        NSString *fullName=[[[[idarray objectAtIndex:i]valueForKey:@"firstName"]stringByAppendingString:@" "]stringByAppendingString:[[idarray objectAtIndex:i]valueForKey:@"lastName"]];
    
        [dict setValue:fullName forKey:@"username"];
        [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"emailId"] forKey:@"emailid"];
        [dict setValue:[[idarray objectAtIndex:i]valueForKey:@"user_id"] forKey:@"userid"];
        
        [[SA_MANAGER sharedManager].arrChat addObject:userChat];
    
        [arrUsers addObject:[dict valueForKey:@"username"]];
    
    
    
 
    
    [_tblChatList reloadData];
    }
        
    }
    

    
-(void)dealloc {
    [searchController.searchBar removeFromSuperview]; // It works!
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
