//
//  ChatViewController.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 13/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"
#import "MBProgressHUD.h"
#import "Chat.h"
#import "SA_MANAGER.h"
#import "MessagesViewController.h"

@interface ChatViewController : UIViewController<UISearchControllerDelegate, UISearchResultsUpdating,UITableViewDelegate,UITableViewDataSource,NSURLConnectionDataDelegate>
{
    NSMutableArray *arrImages;
    NSMutableArray *arrUsers;
    NSMutableArray *arrMessages;
    NSMutableData *dataServer;
    NSMutableArray *idarray;
    NSMutableDictionary *userData;
    NSUInteger selectedrow;
    NSNumber *userNumb;
    UISearchController *searchController;
}

@property(nonatomic,weak) IBOutlet UITableView *tblChatList;
@property(nonatomic,strong) NSString *stringtoPass;
@end
