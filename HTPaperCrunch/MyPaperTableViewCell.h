//
//  MyPaperTableViewCell.h
//  HTPaperCrunch
//
//  Created by HelixTech-User on 21/10/16.
//  Copyright © 2016 HelixTech-User. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface MyPaperTableViewCell : UITableViewCell
{

    BOOL isClicked;

}

@property(nonatomic,strong) IBOutlet UILabel *lblTitle;
@property(nonatomic,strong) IBOutlet UIImageView *imgPaper;
@property(nonatomic,weak)   IBOutlet UIButton *btnDownload;
@property(nonatomic,weak)   IBOutlet UIButton *btnBookmarks;
@property(nonatomic,strong) IBOutlet UILabel *lblExam;
@property (nonatomic,weak) IBOutlet UIButton *btnSeemore;

-(IBAction)btnBookmrakPressed:(id)sender;
-(IBAction)btnSavePressed:(id)sender;

@end
